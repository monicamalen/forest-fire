//#pragma once

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>

#include "Parameters.h"
#include "Individuo.h"



using namespace std;


    Individuo::Individuo()
    {
      	
    }

   	// destructor
    Individuo::~Individuo()
    {
		
    }

	void Individuo::imprimirIndividuo(){

	    cout << fitness << " " << beta_cero << " " << " " << beta_forest << " " ;  
	    cout << beta_aspect << " " << beta_slope  << " " << beta_wind  << " " << x << " " << y  <<  endl ;
	}


	void Individuo::set_values(float beta_cero_in, float beta_forest_in, float beta_aspect_in, float beta_slope_in, float beta_wind_in){
		beta_cero = beta_cero_in;
		beta_forest = beta_forest_in;
		beta_aspect = beta_aspect_in;
		beta_slope = beta_slope_in;
		beta_wind = beta_wind_in;

	}



	void Individuo::set_x(int x_in) {

		x = x_in;
		
	}



	void Individuo::set_y(int y_in) {

		y = y_in;
		
	}


	void Individuo::set_fitness(float fitness_in){

		fitness = fitness_in;

	}
	

	float Individuo::get_fitness(){

		return fitness;
	}



	float Individuo::get_beta_cero() {

		return beta_cero;
	}


	float Individuo::get_beta_forest() {
		return beta_forest;
	}


	float Individuo::get_beta_aspect() {
		return beta_aspect;
	}


	float Individuo::get_beta_slope() {
		return beta_slope;
	}


	float Individuo::get_beta_wind() {
		return beta_wind;
	}

	int Individuo::get_x() {
		return x;
	}
	

	int Individuo::get_y(){

		return y;
	}