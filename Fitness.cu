#include "Fitness.h"
#include <iostream>
#include <sys/time.h> /* gettimeofday */
#include "Parameters_Genetico.h"
#include "curso.h"

using namespace std;

int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);
#define MICROSEC (1E-6)



     
Fitness::Fitness() {
      
}

float Fitness::Calcular_Fitness(int filas, int cols, float *mapa1, float *mapa2, float *h_vege) {
	
	  float fit = 0.0;
          
	  switch (CODIGO_FUNCION_FITNESS) {
	    case 1: // original, del paper del pajaro
		    fit = fitness_secuencial(filas, cols, mapa1, mapa2, h_vege);  
                    break;
		    
	    case 2: // implementado en tesis mdenham
		    fit = fitness_metodo_mdenham_secuencial(filas, cols, mapa1, mapa2);
		
		    break;
	      
            default: break;
	  }
    return fit;
      
}



Fitness::~Fitness() {


      }


// recibe mapa1 valor1 mapa2 valor2 y cuenta cuantas veces (mapa1 ==  valor1) && (mapa2 == valor2)
int Fitness::calcular_interseccion(float *mapa1, float valor1, float *mapa2, float valor2, int filas, int cols)
{
	int tot = 0, i;
	
  	float *mapaReal1, *mapaSim1;
  	mapaReal1 = (float*)malloc(sizeof(float) * filas * cols);
  	mapaSim1 = (float*)malloc(sizeof(float) * filas * cols);

  	HANDLE_ERROR(cudaMemcpy(mapaReal1, mapa1, filas*cols * sizeof(float), cudaMemcpyDeviceToHost));
  	HANDLE_ERROR(cudaMemcpy(mapaSim1, mapa2, filas*cols * sizeof(float), cudaMemcpyDeviceToHost));


  	for(i = 0; i < cols * filas; i++) 
	   	 tot += ((mapaReal1[i] == valor1) && (mapaSim1[i] == valor2));
	

  	free(mapaReal1);
  	free(mapaSim1);
    
    return tot;
}


float Fitness::fitness_secuencial(int filas, int cols, float *mapa1, float *mapa2, float *h_vege)
{
    //float fitness = 0.0;
    int interseccion, A_f, A_m, A_m_sim, A_f_sim;
    

    interseccion = calcular_interseccion(mapa1, 1.0, mapa2, 1.0, filas, cols) + calcular_interseccion(mapa1, 1.0, mapa2, -1.0, filas, cols) + 
                   calcular_interseccion(mapa1, -1.0, mapa2, 1.0, filas, cols) + calcular_interseccion(mapa1, -1.0, mapa2, -1.0, filas, cols)  ;
    // celdas quemadas real con bosque 
    A_f = calcular_interseccion(mapa1,1.0,h_vege,1.0, filas, cols) + calcular_interseccion(mapa1, -1.0, h_vege, 1.0, filas, cols); 
    // celdas quemadas real con matorral 
    A_m = calcular_interseccion(mapa1,1.0, h_vege,2.0, filas, cols) + calcular_interseccion(mapa1, -1.0, h_vege, 2.0, filas, cols);	
    // celdas no combustibles en mapa real
   
    // celdas quemadas en el mapa simulado con matorral
    A_m_sim = calcular_interseccion(mapa2, 1.0, h_vege, 2.0, filas, cols)+calcular_interseccion(mapa2, -1.0, h_vege, 2.0, filas, cols);	
    // celdas quemadas en el mapa simulado con bosque
    A_f_sim = calcular_interseccion(mapa2, 1.0, h_vege, 1.0, filas, cols)+calcular_interseccion(mapa2, -1.0, h_vege, 1.0, filas, cols);		
    // celdas no combustibles en mapa simulado
 

    if(A_f!=0 && A_m!=0)
    {
      return pow(( abs((1-(float)interseccion/(float)(A_f + A_m))) + ((float)abs(A_f - A_f_sim) / (float) A_f) + ((float)abs(A_m - A_m_sim)/ (float)A_m) ),2);
    }
    if(A_f!=0 && A_m==0){return pow(( abs((1-(float)interseccion/(float)(A_f + A_m))) + ((float)abs(A_f - A_f_sim) / (float) A_f) ),2);}
    if(A_m!=0 && A_f==0){return pow(( abs((1-(float)interseccion/(float)(A_f + A_m))) + ((float)abs(A_m - A_m_sim)/ (float)A_m) ),2);}
    if(A_m==0 && A_f==0){return -1;}
 
    return 0.0;  // no debiera llegar nunca por aqui pero es para evitar el warning 
}



// recibe mapa1 valor1 mapa2 valor2 y cuenta cuantas veces (mapa1 ==  valor1) || (mapa2 == valor2)
int Fitness::calcular_union(float *mapa1, float valor1, float *mapa2, float valor2, int filas, int cols)
{
	 int tot = 0, k;
	
	 for(k = 0; k < cols * filas; k++) 
	   	tot += ((mapa1[k] == valor1) || (mapa2[k] == valor2));
	
	 return tot;
}


int Fitness::calcular_celdas_con_valor(float *mapa1, float valor1, int filas, int cols)
{
    int tot = 0, k;
	
	 for(k = 0; k < cols * filas; k++) 
	   	tot += (mapa1[k] == valor1);
	
	 return tot;
  
}


float Fitness::fitness_metodo_mdenham_secuencial(int filas, int cols, float *mapaReal, float *mapaSimulado)
{

    int cantUnion, cantInter, cantReal;

    cantInter = calcular_interseccion(mapaReal, 1.0, mapaSimulado, 1.0, filas, cols)+calcular_interseccion(mapaReal, 1.0, mapaSimulado, -1.0, filas, cols);
    cantUnion = calcular_union(mapaReal, 1.0, mapaSimulado, 1.0, filas, cols)+calcular_union(mapaReal, 1.0, mapaSimulado, -1.0, filas, cols);
    cantReal = calcular_celdas_con_valor(mapaReal, 1.0, filas, cols);

    if(IMPRIMIR)
      cout << "inter: " << cantInter <<  "  union: " << cantUnion << "  real: " << cantReal << endl;


    return (float)((cantUnion-1) - (cantInter-1)) / (float)(cantReal-1);

}


/***************************************************************/
/* FITNESS PARALELO  MDENHAM                                        */
 /***************************************************************/

/* kernel que accede a los datos y calcula union, interseccion y cantidad de celdas del mapa real*/
__global__ void kernel_calcular_valores(float *mapaReal, float valor1, float *mapaSimulado, float valor2, float valor2bis, int filas, int cols, int *d_cantidades) {

     int tcol = threadIdx.x + (blockIdx.x * blockDim.x) ;
     int tfil = threadIdx.y + (blockIdx.y * blockDim.y) ;
     int tid = (tfil * cols) + tcol;
     
     float real_v = mapaReal[tid];
     float sim_v = mapaSimulado[tid];

        
     /* ATENCION: d_cantidades = [0] <- cant_Intersecion, [1] <- cant union, [2] <- cant real*/   
     if ((tcol < cols) && (tfil < filas)) {

        if ( ((real_v == valor1) && (sim_v == valor2))       || 
             ((real_v == valor2bis) && (sim_v == valor2bis))   ||
             ((real_v == valor1) && (sim_v == valor2bis))    || 
             ((real_v == valor2bis) && (sim_v == valor2)) ) 
                    atomicAdd(&(d_cantidades[0]), 1); 

        if ( ((real_v == valor1) || (sim_v == valor2)) || 
             ((real_v == valor2bis) || (sim_v == valor2bis))	  || 
             ((real_v == valor2bis) || (sim_v == valor2)) || 
             ((real_v == valor1) || (sim_v == valor2bis))  
           )
                   atomicAdd(&(d_cantidades[1]), 1);
                 
        if (mapaReal[tid] == valor1 || mapaReal[tid] == valor2bis)
            atomicAdd(&(d_cantidades[2]),1);
     }

}


 //int calcular_todos_los_valores_paralelos(float *mapaReal, int valor1, float *mapaSimulado, int valor2, int filas, int cols, int *cantInter, int *cantUnion, int *cantReal) {
int calcular_todos_los_valores_paralelos(float *mapaReal, int valor1, float *mapaSimulado, int valor2, int valor2bis, int filas, int cols, int *d_cantidades) {

    int total = 0;
    dim3 nThreads(32,32);
    dim3 nBlocks(cols/nThreads.x + (cols % nThreads.x ? 1:0), filas/nThreads.y + (filas % nThreads.y ? 1:0));
 
  
    kernel_calcular_valores<<<nBlocks, nThreads>>>(mapaReal, valor1, mapaSimulado, valor2, valor2bis, filas, cols, d_cantidades);

    cudaDeviceSynchronize();

    checkCUDAError("Launch failure: ");
    return total;
}






float fitness_metodo_mdenham_paralelo(int filas, int cols, float *mapaReal, float *mapaSimulado) 
{

   int *h_cantidades = (int*)malloc(sizeof(int) * 3);
   int *d_cantidades;


   HANDLE_ERROR(cudaMalloc((void**)&d_cantidades, sizeof(int)*3));
   cudaMemset(d_cantidades, 0, sizeof(int) * 3);


   
   calcular_todos_los_valores_paralelos(mapaReal, 1.0, mapaSimulado, 1.0, -1.0, filas, cols, d_cantidades);
   //los que se apagaron -1 y los que siguen prendidos 

   // me traigo el vector calculado
   HANDLE_ERROR(cudaMemcpy(h_cantidades, d_cantidades, sizeof(int) * 3, cudaMemcpyDeviceToHost));


   
   float resu = (float)((float)((h_cantidades[1] ) - (h_cantidades[0] )) / (float)(h_cantidades[2])); //- (h_cantidades[2]>1?1:0)));
   
   return resu;

}






#include <thrust/device_ptr.h>
#include "overlaps.h"
float fitness_mdenham_paralelo_reduce(int filas, int cols, float *mapaReal, float *mapaSimulado) 
{

    thrust::device_ptr<float> d_mapaReal=thrust::device_pointer_cast(mapaReal); 
    thrust::device_ptr<float> d_mapaSimulado=thrust::device_pointer_cast(mapaSimulado);  

    Union opunion;
    float res_union = operacion(d_mapaReal,d_mapaSimulado,opunion, filas*cols);

    Interseccion opintersec;
    float res_intersec = operacion(d_mapaReal,d_mapaSimulado,opintersec,filas*cols);

    float area_real_quemada= operacion(d_mapaReal,d_mapaReal,opintersec,filas*cols); 

   
    return  (float)(res_union - res_intersec) / (float) area_real_quemada;


}


/************************************************************/
/*  FITNESS PARALELO PAJARO                                 */
/************************************************************/

/* kernel que accede a los datos y calcula union, interseccion y cantidad de celdas del mapa real*/
__global__ void kernel_calcular_valores_pajaro_paralelo(float *mapaReal, float valor1, float *mapaSimulado, float valor2, float valor2bis, float * mapaVegetacion, int filas, int cols, int *d_valores) {

     int tcol = threadIdx.x + (blockIdx.x * blockDim.x) ;
     int tfil = threadIdx.y + (blockIdx.y * blockDim.y) ;
     int tid = (tfil * cols) + tcol;

     float real_v = mapaReal[tid];
     float sim_v = mapaSimulado[tid];
     float vege_v = mapaVegetacion[tid];
        
     /* ATENCION: d_valores [0] <- interseccino mapa real y simulado -> interseccion
                  d_valores [1] <- mapa real quemado con matorral (d_vege[i] = 2.0) -> A_m 
                  d_valores [2] <- mapa real quemado con bosque (d_vege [i] = 1.0 -> A_f 
                  d_valores [3] <- mapa real quemado con vege[i] = 0 , 
                  d_valores [4] <- mapa simulado quemado con matorral (d_vege[i] = 2.0) -> A_m_sim
                  d_valores [5] <- mapa simulado quemado con bosque (d_vege [i] = 1.0 -> A_f_sim
                  d_valores [6] <- mapa simulado quemado con vege[i] = 0 , 
        */   

     if ((tcol < cols) && (tfil < filas)) {
      
            /*   INTERSECCION    */
        if ( ((real_v == valor1) && (sim_v == valor2))       || 
             ((real_v == valor2bis) && (sim_v == valor2bis))   ||
             ((real_v == valor1) && (sim_v == valor2bis))    || 
             ((real_v == valor2bis) && (sim_v == valor2)) )
                    atomicAdd(&(d_valores[0]), 1); 

            /*   QUEMADO REAL CON MATORRAL   A_m  */
        if ( ((real_v == valor1) && (vege_v == 2.0)) ||        
             ((real_v == valor2bis) && (vege_v == 2.0))  )
                   atomicAdd(&(d_valores[1]), 1);
                     

            /*    QUEMADO REAL CON BOSQUE   A_f*/
        if ( ((real_v == valor1) && (vege_v == 1.0)) || 
             ((real_v == valor2bis) && (vege_v == 1.0))  )
                   atomicAdd(&(d_valores[2]), 1);
 

            /*    QUEMADO REAL CON VEGETACION = 0.0  */
        if ( ((real_v == valor1) && (vege_v == 0.0)) || 
             ((real_v == valor2bis) && (vege_v == 0.0))  )
                  atomicAdd(&(d_valores[3]), 1);


            /*   QUEMADO SIMULADO CON MATORRAL     */
        if ( ((sim_v == valor2) && (vege_v == 2.0)) ||        
             ((sim_v == valor2bis) && (vege_v == 2.0))  )
                  atomicAdd(&(d_valores[4]), 1);
 

            /*    QUEMADO REAL CON BOSQUE */
        if ( ((sim_v == valor2) && (vege_v == 1.0)) || 
             ((sim_v == valor2bis) && (vege_v == 1.0))  )
                   atomicAdd(&(d_valores[5]), 1);
   

            /*    QUEMADO SIMULADO CON VEGETACION = 0.0  */
        if ( ((sim_v == valor2) && (vege_v == 0.0)) || 
             ((sim_v == valor2bis) && (vege_v == 0.0))  )
                   atomicAdd(&(d_valores[6]), 1);
         
        }

}



void calcular_valores_pajaro_paralelo(int filas, int cols, float *mapa_real, float *mapa_simulado, float * d_vege, int *d_valores)
{

    dim3 nThreads(32,32);
    dim3 nBlocks(cols/nThreads.x + (cols % nThreads.x ? 1:0), filas/nThreads.y + (filas % nThreads.y ? 1:0));
 
  
     kernel_calcular_valores_pajaro_paralelo<<<nBlocks, nThreads>>>(mapa_real, 1.0, mapa_simulado, 1.0, -1.0, d_vege, filas, cols, d_valores);
     if ( cudaSuccess != cudaGetLastError() )
          printf( "Error!\n" );


    cudaDeviceSynchronize();

    
}
// funcion del fitness del pajaro

float fitness_metodo_pajaro_paralelo(int filas, int cols, float *mapa1, float *mapa2, float *d_vege) {

    int *h_valores;
    h_valores = (int*)malloc(sizeof(int) * 7);

    int *d_valores;
    cudaMalloc((void**)&d_valores, sizeof(int) * 7);
    cudaMemset(d_valores, 0, sizeof(int) * 7);

   

    calcular_valores_pajaro_paralelo(filas, cols, mapa1, mapa2, d_vege, d_valores);


    cudaMemcpy(h_valores, d_valores, 7 * sizeof(int), cudaMemcpyDeviceToHost);
    
    int interseccion = h_valores[0];
    int A_f = h_valores[2];
    int A_m = h_valores[1];
    int A_f_sim = h_valores[5];
    int A_m_sim = h_valores[4];
    
    free(h_valores);
    cudaFree(d_valores);


     if(A_f!=0 && A_m!=0)
    {
      return pow(( abs((1-(float)interseccion/(float)(A_f + A_m))) + ((float)abs(A_f - A_f_sim) / (float) A_f) + ((float)abs(A_m - A_m_sim)/ (float)A_m) ),2);
    }

    if(A_f!=0 && A_m==0){return pow(( abs((1-(float)interseccion/(float)(A_f + A_m))) + ((float)abs(A_f - A_f_sim) / (float) A_f) ),2);}
    if(A_m!=0 && A_f==0){return pow(( abs((1-(float)interseccion/(float)(A_f + A_m))) + ((float)abs(A_m - A_m_sim)/ (float)A_m) ),2);}
    if(A_m==0 && A_f==0){return -1;}
 

    if(A_f!=0 && A_m==0){return pow(( abs((1-(float)interseccion/(float)(A_f + A_m))) + ((float)abs(A_f - A_f_sim) / (float) A_f) ),2);}
    if(A_m!=0 && A_f==0){return pow(( abs((1-(float)interseccion/(float)(A_f + A_m))) + ((float)abs(A_m - A_m_sim)/ (float)A_m) ),2);}
    if(A_m==0 && A_f==0){return -1;}

    
}



float Fitness::Calcular_Fitness_paralelo(int filas, int cols, float *d_mapa1, float *d_mapa2, float *d_vege)
//double Fitness::Calcular_Fitness_paralelo(int filas, int cols, float *d_mapa1, float *d_mapa2, float *d_vege)
{
    // tengo el mapa de referencia en d_reference_map
    float fit;

    switch (CODIGO_FUNCION_FITNESS) {
    
        case 1: // original, del paper del pajaro


            fit = fitness_metodo_pajaro_paralelo(filas, cols, d_mapa1, d_mapa2, d_vege);  

            break;    

        case 2: // implementado en tesis mdenham

  
            fit = fitness_metodo_mdenham_paralelo(filas, cols, d_mapa1, d_mapa2); 

            break;

        case 3:

          // esto sería una prueba para testear la funcion de fitness implementada en la tesis de Monica Denham
          // implementada de dos formas: con reduce (thrust) y con sumas atómicas
   
          fit = fitness_mdenham_paralelo_reduce(filas, cols, d_mapa1, d_mapa2);

          break;

        default: break;
    }

    return fit;
  }



