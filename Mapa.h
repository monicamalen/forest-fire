//#pragma once
#include "Parameters.h"
#include "Lista_Campeones.h"
#include "Fitness.h"
#include <vector>
#include "Poblacion.h"
#include "Parameters_Genetico.h"

      struct DatosCelda {
		int step;
		int timeFireOn; //tiempo en el que se inicia el fuego en una celda.
		float intensity;
		int fireOrigin; // celda vecina desde donde vino el fuego
	};


/* Declaración de la clase Mapa */
class Mapa
{
    private:
	
	std::vector<int> h_indicesfuegoRef;	


	// lista donde se mantienen los N mejores individuos
	Lista_Campeones campeones;
	Fitness fitness;
	
	// float array to hold fire map
	float *h_terrain;
	float *d_terrain, *d_terrainnew;
 	// float array to hold environmental features
	float *h_slope, *h_aspect, *h_wind, *h_forest, *h_alti, *h_real_fire, *h_vege;
	float *d_slope, *d_aspect, *d_wind, *d_forest, *d_alti, *d_real_fire, *d_vege;
	float *d_reference_map;  // mapas de referencia para calcular el fitness (en cpu y gpu para hacerlo secuencial o paralelo)
	float **h_reference_map; //cada puntero apunta a una mapa de referencia
	//Propagation Probabilities Map
	float *d_pmap, *h_pmap;

	int *h_fuego_propaga;
	int *d_fuego_propaga;	

        // private functions
	void CpyHostToDevice(float *d_mapa, float *h_mapa, int filas, int cols );
	void CpyDeviceToHost(float *h_mapa, float *d_mapa, int filas, int cols );
	void GetMap(const char * nombre, float *mapa, int nx, int ny);
	void SaveMap(char * nombre, float *mapa, int filas, int cols);
	int filas,cols;
	
	void Iniciar_Punto_de_Ignicion_Mapa_Sintetico_de_Referencia(int *x, int *y,int nref);  //funcion privada
	void Obtener_coordenadas_fuego_referencia(int nref);


    public:
	// mapa con datos sobre la ignición del mapa
	DatosCelda *h_mapa_datos, *d_mapa_datos;
	Mapa();
	Mapa(int nx, int ny);
	~Mapa();

	void Initmaps(int filas, int cols, int seed_, int *filaIni, int *colIni, std::vector<int> &h_indicesfuegoRef );
	void  Init_Reference_Map(int *x, int *y, int modulo, int nada, int nreferencia, vector<int> &h_indices_referencia);
	void Init_All_Maps(int filas, int cols);
	void Init_Initial_Map(int *x, int *y);
	void InitFire();
	
	/* Funciones que popagan el fuego llamando a un kernel. Saco los datos de distintos lugares por eso son dos.  */ 
	void Fire_Propagation(int i, int *h_fuego_propaga, int *d_fuego_propaga, int verificar_propagacio, Individuo_T *d_poblacion, int indice_individuo);
	void Fire_Propagation_Fuego_de_Referencia(int semilla, Individuo_T individuo);

	void print_fire_progress(int iteration);
	void print_probability_progress(int iteration);
	
	void Inicializar_datos_celda(DatosCelda *h_mapa_datos);
	float calcular_fitness_secuencial(int filas, int cols);
	float calcular_fitness_paralelo(int filas, int cols);
	void PrintList(char * nombre, float fitness);
	void PrintParamsRef(char * nombre, int x, int y,Individuo_T individuo_params_iniciales);
	void PrintParamsSims(char * nombre, int x, int y);
	// agrega a campeones sii el fitness es mayor al peor fitness de la lista
	void agregar_a_campeones(float fitness, int x, int y);
	void ImprimirCampeonesEnArchivo();
	void FireProbabilityMap(int tcurrent_run);

	/* funciones para el Algoritmo Genetico */
	int Evaluar_Individuo(Individuo_T *d_poblacion, int indice, int flag_imprimir_mejor_mapa, int numero_generacion);  // este ultimo parametro es para simular con el mejor individuo
	int Evaluar_Poblacion(Individuo_T *d_poblacion, Fitness_T *h_fitnesses, int numero_generacion);

	
};



