//#pragma once
#include "Parameters.h"
#include<fstream>
#include <map>

using namespace std;

/* Declaración de la clase Lista_Campeones */

class Lista_Campeones
{
    private:
	// variables privadas
	int size;	

	// estructuras privadas
	// cada registro de la lista es una combinación de parametros + fitness	
	typedef struct {
 	   float fitness;
	   float beta_cero,beta_forest,beta_aspect,beta_slope,beta_wind;
	   int x,y;
	}Individuo_T;


	multimap<float, Individuo_T> lista;

	
	
    public:
      	// constructores
	Lista_Campeones();
	
      	// destructor
	~Lista_Campeones();

	// funciones publicas
	void imprimirLista(); 
	void imprimirListaEnArchivo();
	
	int HayQueAgregarIndividuo(float fitness);
        void agregar(float fitness, float beta_cero, float beta_forest, float beta_aspect, float beta_slope, float beta_wind, int x, int y);
	ofstream fout; 
};



