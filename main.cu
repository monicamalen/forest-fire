#include <cmath>
#include <assert.h>
#include <sys/time.h>	/* gettimeofday */

#include <cuda.h>
#include <cuda_runtime.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/swap.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include <thrust/system_error.h>
#include <thrust/copy.h>


#include <stdio.h>	/* print */
#include <stdlib.h>
#include <iostream>	/* print */
#include <math.h>   /*math functions stl library*/
#include <algorithm>  /* std::min_element, std::max_element*/
#include <vector>
// Including our functions and parameters
#include "Mapa.h"
#include "Parameters.h"
#include "Parameters_Genetico.h"
#include "histo.h" /*Para hacer histogramas a partir de un vector de reales*/
#include "Poblacion.h"
#include "BitonicSort.h"
#include "curso.h"

#define NROBINS  100  // numero de bins del histograma de fitness
#define MICROSEC (1E-6)


int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y);


int fitness_minimo(Fitness_T *h_fitnesses, float *min, int *individuo_min);


int main(int argc, char **argv){
  
      
	double secs = 0.0;
	
	struct timeval start = {0L,0L}, end = {0L,0L}, elapsed = {0L,0L};

	int size_poblacion = sizeof(Individuo_T) * CANT_INDIVIDUOS;
	int size_fitnesses = sizeof(Fitness_T) * CANT_INDIVIDUOS;

	if (argc == 1) {
		printf("Usar main semilla_poblacion \n");
		exit(-1);
	}

	int semilla_poblacion = atoi(argv[1]);
	
	// para tener las celdas del fuego de referencia
	std::vector<int> h_indices_fuego_referencia;
	h_indices_fuego_referencia.clear();	

/*	char *nombre_salida = argv[1];*/
	
	/* datos de la placa */
	int card;
	cudaGetDevice(&card);
	cudaDeviceProp deviceProp;
	cudaGetDeviceProperties(&deviceProp, card);
	std::cout << "\nDevice Selected " << card << " " << deviceProp.name << "\n";
	printf("Estoy usando la placa: %d, %s\n",card,deviceProp.name);


	/* Vector de individuos = poblacion en host y device */
	Individuo_T *h_poblacion, *d_poblacion;
	h_poblacion = (Individuo_T*)malloc(size_poblacion);
	HANDLE_ERROR(cudaMalloc((void**)&d_poblacion, size_poblacion));

	Fitness_T *h_fitnesses, *d_fitnesses;
	h_fitnesses = (Fitness_T*)malloc(size_fitnesses);
	HANDLE_ERROR(cudaMalloc((void**)&d_fitnesses, size_fitnesses));
		
	// coordenadas del fuego inicial
	int x,y;
	int i;
		
	// Start timer
	gettimeofday(&start, NULL);
	
	//armar un vector_distancias con todas las fit
	thrust::host_vector<float> v_distancias(NTOT_SIMS);
	Mapa M(ROWS, COLS); //aloco memoria para los mapas que voy a usar
		
	
//	int id=0; //indice de distancias

	int modulo = 0;

	/* MAPAS DE REFERENICA */ 
	M.Init_All_Maps(ROWS, COLS); //aloco memoria para todos los mapas y calculo psi.
	


	for(int nref = 0; nref < CANT_MAPAS_REFERENCIA; nref++) //voy a promediar sobre PROMEDIOS_REFMAP mapas iniciales
	{
		/*Inicializo mapa de referencia, que puede ser el mapa real del incendio o bien un mapa sintetico generado con parametros y pto de ignicion conocido o desconocido*/
		
		M.Init_Reference_Map(&x, &y, modulo, 0, nref, h_indices_fuego_referencia); //modulo es para imprimir los mapas de referencia 0-modulo 

		/*//y 0 es el parametro verificar_propagacion, en este caso no quiero que pare. nref indica el numero de mapas de referencia que voy a usar
		M.PrintParamsRef("paramsfuegoINICIAL.dat",x,y); //x,y pudieron haberse modificado por eso los imprimo*/
	  
	}
	
	/* almaceno las celdas quemadas en el fuego de referencia, los necesito para mutacion en el genetico*/
	thrust::device_vector<int> d_indices_fuego_referencia = h_indices_fuego_referencia;


	/* GENERAR POBLACION  */   
	Poblacion pobla;

	

	switch (DISTRIB_PRIOR) {

			case 1: 
					pobla.Inicializar_Poblacion_Cercana_Best_Params(h_poblacion);
			 		break;
			case 2: 
					pobla.Inicializar_Poblacion_Random(h_poblacion, semilla_poblacion, h_indices_fuego_referencia);
					break;
			case 3: 
					pobla.Inicializar_Poblacion_Gausiana(h_poblacion,semilla_poblacion, h_indices_fuego_referencia);
					break;
			case 4: 
					pobla.Inicializar_Poblacion_Exponencial(h_poblacion,semilla_poblacion, h_indices_fuego_referencia);
					break;		
			case 5: 
					pobla.Inicializar_Poblacion_Best_Params(h_poblacion,semilla_poblacion, h_indices_fuego_referencia);
					break;	
			default:
					pobla.Inicializar_Poblacion_Random(h_poblacion, semilla_poblacion, h_indices_fuego_referencia);  // si es cualq numero inicializamos poblacion random
					break;
	}


	/* ESCRIBIR POBLACION EN ARCHIVO */
	pobla.Imprimir_en_Archivo(POBLACION_INICIAL, h_poblacion);
		
	// para empezar a operar la poblacion se manda a gpu y queda ahi
	pobla.Enviar_Poblacion_a_GPU(h_poblacion, d_poblacion);

	

	if (CODIGO_ALGORITMO_BUSQUEDA == 1) { // 1 -> GA
	

		
		int generacion;

		for (generacion = 0; generacion < CANT_EVOLUCIONES-1; generacion++) {


		//	cout << "Evaluacion de individuos, generacion: " << generacion << endl;
			M.Evaluar_Poblacion(d_poblacion, h_fitnesses,generacion);  // simula + fitness

			if (IMPRIMIR_FITNESS_POR_EVOLUCION){
				
				float min;
				int individuo_min;
				fitness_minimo(h_fitnesses, &min, &individuo_min);

				cout << "Generacion: " << generacion << " Fitness: " << min << " (" << individuo_min << ")" << endl;

			}

			/* mando los fitness a gpu  */
			HANDLE_ERROR(cudaMemcpy(d_fitnesses, h_fitnesses, size_fitnesses, cudaMemcpyHostToDevice));

			/* ESTO ES PARA IMPRIMIR EN CADA PASO LOS MEJORES 100 INDIVIDUOS */
			if (IMPRIMIR_CADA_POBLACION) {

				/* me traigo la poblacion */
				Individuo_T * h_pobla_aux, individuo_aux;
				h_pobla_aux = (Individuo_T*)malloc(sizeof(Individuo_T) * CANT_INDIVIDUOS);
				HANDLE_ERROR(cudaMemcpy(h_pobla_aux, d_poblacion, sizeof(Individuo_T) * CANT_INDIVIDUOS, cudaMemcpyDeviceToHost));


				BitonicSort orden;  

				// no ordeno el mismo porque despues me cambia el resto de operaciones
				Fitness_T *d_fitnesses_aux, *h_fitnesses_aux;

				h_fitnesses_aux = (Fitness_T*)malloc(sizeof(Fitness_T) * CANT_INDIVIDUOS);
				HANDLE_ERROR(cudaMalloc((void**)&d_fitnesses_aux, sizeof(Fitness_T) * CANT_INDIVIDUOS));

				HANDLE_ERROR(cudaMemcpy(d_fitnesses_aux, d_fitnesses, sizeof(Fitness_T) * CANT_INDIVIDUOS, cudaMemcpyDeviceToDevice));

				orden.Sort(d_fitnesses_aux);
				HANDLE_ERROR(cudaMemcpy(h_fitnesses_aux, d_fitnesses_aux, sizeof(Fitness_T) * CANT_INDIVIDUOS, cudaMemcpyDeviceToHost));

				for (i=0; i < 10; i++) {
					individuo_aux = h_pobla_aux[h_fitnesses_aux[i].id];
					cout << "(" << h_fitnesses_aux[i].id << ") \t-> " << h_fitnesses_aux[i].fitness << "\t" << 
					               individuo_aux.id << "\t" << individuo_aux.beta_cero << "\t" << individuo_aux.beta_forest << "\t" << 
					               individuo_aux.beta_slope << "\t" << individuo_aux.beta_aspect << "\t" << individuo_aux.beta_wind <<
					                "\t" << individuo_aux.x << "\t"  <<  individuo_aux.y << endl; 
				}

				free(h_fitnesses_aux);
				cudaFree(d_fitnesses_aux);
				free(h_pobla_aux);
			}


			pobla.Pasos_evolucion(d_poblacion, d_fitnesses, generacion, d_indices_fuego_referencia);

		//	printf("Evolucion: %d \n", generacion);

		}
	
	//	cout << "Evaluacion de individuos, generacion: " << generacion << endl;
		M.Evaluar_Poblacion(d_poblacion, h_fitnesses,CANT_EVOLUCIONES-1);  // simula + fitness
	} // termian el GA 
	else 		
		if (CODIGO_ALGORITMO_BUSQUEDA == 0) {// fuerza bruta

			M.Evaluar_Poblacion(d_poblacion, h_fitnesses,0);  // simula + fitness

			/* mando los fitness a gpu  */
			HANDLE_ERROR(cudaMemcpy(d_fitnesses, h_fitnesses, size_fitnesses, cudaMemcpyHostToDevice));

		}
		

		/* toma de tiempos */
		gettimeofday(&end,NULL);
		timeval_subtract(&elapsed, &end, &start);
		secs = (double)elapsed.tv_sec + ((double)elapsed.tv_usec*MICROSEC);
  		printf("Terrain size: nx=%d NY=%d. Fire Simulation execution time (in hours): %lf, TRUN=%d, NRUNS=%d \n", ROWS, COLS, secs/3600,T_RUN,NTOT_SIMS);



		if (IMPRIMIR_POBLACION_FINAL) {

		
			FILE *output = fopen(POBLACION_SALIDA,"w");
			BitonicSort orden;

			if (output == NULL)
			{
				printf("Imprimiendo la poblacion final error: no abre archivo %s \n", POBLACION_SALIDA);
				exit(-1);
			}


			// ordeno poblacion e imprimimo todos los invididuos en orden de mejor a pero fitness

				// no ordeno el mismo porque despues me cambia el resto de operaciones
				Fitness_T *d_fitnesses_aux, *h_fitnesses_aux;

				h_fitnesses_aux = (Fitness_T*)malloc(sizeof(Fitness_T) * CANT_INDIVIDUOS);
				HANDLE_ERROR(cudaMalloc((void**)&d_fitnesses_aux, sizeof(Fitness_T) * CANT_INDIVIDUOS));

				HANDLE_ERROR(cudaMemcpy(d_fitnesses_aux, d_fitnesses, sizeof(Fitness_T) * CANT_INDIVIDUOS, cudaMemcpyDeviceToDevice));

				orden.Sort(d_fitnesses_aux);
				HANDLE_ERROR(cudaMemcpy(h_fitnesses_aux, d_fitnesses_aux, sizeof(Fitness_T) * CANT_INDIVIDUOS, cudaMemcpyDeviceToHost));


				// me traigo la poblacion
				HANDLE_ERROR(cudaMemcpy(h_poblacion, d_poblacion, sizeof(Individuo_T) * CANT_INDIVIDUOS, cudaMemcpyDeviceToHost));

				fprintf(output,"#fitness  beta_cero  beta_forest  beta_aspect  beta_slope  beta_wind xinicial yinicial \n");
				int id;
				for (i=0; i < CANT_INDIVIDUOS; i++) {
				//	cout << "(" << h_fitnesses_aux[i].id << ") -> " << h_fitnesses_aux[i].fitness << endl; 

					id = h_fitnesses_aux[i].id;
					
					fprintf(output,"%f %f %f %f %f %f %d %d \n",h_fitnesses_aux[i].fitness, h_poblacion[id].beta_cero, h_poblacion[id].beta_forest, h_poblacion[id].beta_aspect, h_poblacion[id].beta_slope, h_poblacion[id].beta_wind, h_poblacion[id].x, h_poblacion[id].y); 

				
				}

				// me guardo el mejor individuo para imprimir el mapa
				id = h_fitnesses_aux[0].id;

				//Individuo_T champion = h_poblacion[id];
				int flag_imprimir_mapa = 1;
				M.Evaluar_Individuo(d_poblacion, id, flag_imprimir_mapa,CANT_EVOLUCIONES-1); // ultimo nro es un flag para que imprima el mapa despues de evaluar el individou


				free(h_fitnesses_aux);
				cudaFree(d_fitnesses_aux);

				fclose(output);
			
			} // imprimir poblacion final 

	

	

	

	free(h_poblacion);
	free(h_fitnesses);
	cudaFree(d_poblacion);
	cudaFree(d_fitnesses);

	return 0;
}




int fitness_minimo(Fitness_T *h_fitnesses, float *min, int *individuo_min) {


	int i;

	float minActual = 999999.99;
	int indi;

	for (i = 0; i < CANT_INDIVIDUOS;  i++) {
		if (h_fitnesses[i].fitness < minActual) {
			minActual = h_fitnesses[i].fitness;
			indi =  h_fitnesses[i].id;
		}		
	}


	*min = minActual;
	*individuo_min = indi;

	return 0;

}



/*
 * http://www.gnu.org/software/libtool/manual/libc/Elapsed-Time.html
 * Subtract the `struct timeval' values X and Y,
 * storing the result in RESULT.
 * Return 1 if the difference is negative, otherwise 0.
 */

int timeval_subtract (struct timeval *result, struct timeval *x, struct timeval *y) {
	/* Perform the carry for the later subtraction by updating y. */
	if (x->tv_usec < y->tv_usec) {
		int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
		y->tv_usec -= 1000000 * nsec;
		y->tv_sec += nsec;
	}
	if (x->tv_usec - y->tv_usec > 1000000) {
		int nsec = (x->tv_usec - y->tv_usec) / 1000000;
		y->tv_usec += 1000000 * nsec;
		y->tv_sec -= nsec;
	}

	/* Compute the time remaining to wait. tv_usec is certainly positive. */
	result->tv_sec = x->tv_sec - y->tv_sec;
	result->tv_usec = x->tv_usec - y->tv_usec;

	/* Return 1 if result is negative. */
	return x->tv_sec < y->tv_sec;
}

