#pragma once

  /* Este tipo lo necesito en todos lados por eso lo pongo acá*/
   typedef struct {
          int id;
          float fitness;
          float beta_cero,beta_forest,beta_aspect,beta_slope,beta_wind;
          int x,y;
       }Individuo_T;  


  /* Este tipo lo necesito en todos lados por eso lo pongo acá*/
   typedef struct {
          int id;
          float fitness;
       }Fitness_T;  


/* GENETICO */
#ifndef CANT_INDIVIDUOS 
#define CANT_INDIVIDUOS 10 /*NRO total de simulaciones*/
#endif

#ifndef CANT_EVOLUCIONES 
#define CANT_EVOLUCIONES 3 /*NRO total de simulaciones*/
#endif

#ifndef POBLACION_INICIAL 
#define POBLACION_INICIAL "poblacion_inicial.dat"
#endif

#ifndef CROSSOVER_PROBABILITY
#define CROSSOVER_PROBABILITY 0.90
#endif

#ifndef MUTATION_PROBABILITY
#define MUTATION_PROBABILITY 0.90
#endif

#ifndef ELITISM_PERCENTAGE
#define ELITISM_PERCENTAGE 30
#endif

#ifndef ELITISM
#define ELITISM 1    // 1 para que haya elitismo, 0 para que no. 
#endif

#ifndef X_Y_MUTANTES
#define X_Y_MUTANTES 0  
#endif

#ifndef CANT_RANGOS_MUTACION_DINAMICA
#define CANT_RANGOS_MUTACION_DINAMICA 4     //Nro de Etapas en que va bajando la probabilidad de Mutacion ver mutar_poblacion en Genetico.cu
#endif

#ifndef POBLACION_SALIDA
#define POBLACION_SALIDA "poblacion_final.dat"
#endif

#ifndef IMPRIMIR_POBLACION_FINAL
#define IMPRIMIR_POBLACION_FINAL 1     // para imprimir los campeones 
#endif

#ifndef IMPRIMIR_FITNESS_POR_EVOLUCION
#define IMPRIMIR_FITNESS_POR_EVOLUCION 0     //1 para imprimir en pantalla los campeones 
#endif
       
