#include<stdlib.h>

double box_muller()
{
	  double u1 = (random()*1.0)/RAND_MAX;
	  double u2 = (random()*1.0)/RAND_MAX;
	  double r = sqrt( -2.0*log(u1) );
	  double theta = 2.0*M_PI*u2;
	return r*sin(theta);
}
