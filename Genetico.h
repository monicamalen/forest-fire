#pragma once

#include "Parameters_Genetico.h"
#include "Parameters.h"


class Genetico
{
    private:
	
    public: 
	int Evolucionar_Poblacion(Fitness_T *d_fitnesses, Individuo_T *d_poblacio, int nro_generacion,  thrust::device_vector<int> d_indices_fuego_referencia);

};