//#pragma once

#include <iostream>
#include <fstream>
#include <stdio.h>
#include <string.h>

#include "Parameters.h"
#include "Lista_Campeones.h"


#include <thrust/swap.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>

using namespace std;

//using namespace std;

    Lista_Campeones::Lista_Campeones()
    {
      	// creo el archivo donde voy a escribir los mejores fitness
      	fout.open("paramsfitxy.dat");
	    
	    // imprimo el encabezamiento del archivo donde iran los mejores fitness
	      fout << "fitness" << " "  << "beta_cero" << " "<<"beta_forest"<<" " << "beta_aspect"<<" " <<"beta_slope"<<" " 
		<< "beta_wind"<<" " << "xinicial"<<" " << "yinicial"<<endl ;
    }


      	// destructor
    Lista_Campeones::~Lista_Campeones()
    {
		lista.clear(); // limpia todos los elementos de la lista
		fout.close();
    }

	
    int Lista_Campeones::HayQueAgregarIndividuo(float fit)
    {
		// si hay lugar en la lista agrego si o si (return 1) sino me fijo si el fitness 
		// es mejor que el peor de la lista
		if (lista.size() < MAX_DIM_CAMPEONES)
	  		return 1;
		else
		{
	 		// creo un reverse iteratior y con rbegin me voy al ultimo elemento. Ahi le pido el first (fitness)
	  		map<float, Individuo_T>:: reverse_iterator riter;
	  		riter = lista.rbegin();
	  		riter--;
	  		return (riter->first > fit);
		}  
    }
    

    
    void Lista_Campeones::agregar(float fitness, float beta_cero, float beta_forest, float beta_aspect, float beta_slope, float beta_wind, int x, int y)
    {
      // armo el próximo individuo a agregar
        Individuo_T nuevo = {fitness, beta_cero, beta_forest, beta_aspect, beta_slope, beta_wind, x, y};
     
	    // mantengo siempre la misma dimensión de MAX_DIM_CAMPEONES por lo que debo eliminar el último elemento        
		if (lista.size() == MAX_DIM_CAMPEONES)
		{
	  		map<float, Individuo_T>:: iterator iter;
         	// busco el ultimo (peor fitness lo saco de campeones)
          	iter = lista.end();
	  		iter--;
	 		lista.erase(iter);  // elimino el último
		}
	
		// agrego el elemento
		lista.insert(std::pair<float, Individuo_T>(fitness, nuevo));  	  
    
    }      
    
  
    
    void Lista_Campeones::imprimirLista()
    {
          //map<string, MySet> index_; -> lista
	    map<float, Individuo_T>::iterator iter;
	    for (iter = lista.begin(); iter != lista.end(); iter++) {
		cout << "!!! " << iter->first << " " << iter->second.beta_cero << " " << iter->second.beta_aspect<<endl ;
	    }

    }
      
    void Lista_Campeones::imprimirListaEnArchivo()
    {
        // crea un iterador para usar en lista (lista de campeones de clase map)
	    map<float, Individuo_T>::iterator iter;
	 
	 	// imprime en archivo fout
	    for (iter = lista.begin(); iter != lista.end(); iter++) {
			fout << iter->first << " "  << iter->second.beta_cero << " "<<
			iter->second.beta_forest<<" " << iter->second.beta_aspect<<" " << iter->second.beta_slope<<" " 
			<< iter->second.beta_wind<<" " << iter->second.x<<" " << iter->second.y<<endl ;
	          
	    }

    }
