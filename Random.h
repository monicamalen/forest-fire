#pragma once

#include "Parameters_Genetico.h"
#include "Parameters.h"

// RNG: PHILOX
#include "RNGcommon/Random123/philox.h"
#include "RNGcommon/Random123/u01.h"

class Genetico
{
    private:
	
    public: 


typedef r123::Philox2x64 RNG2;
typedef r123::Philox4x64 RNG4;

#define SEED 1332277027LLU   /*semilla para Philox*/

	/*Funcion generadora de numeros aleatorios en paralelo*/
	__device__
	void PhiloxRandomPair(const unsigned int index, const unsigned int position, float *r1, float *r2);


};