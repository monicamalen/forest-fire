//#pragma once
#include "Parameters.h"
#include <fstream>

using namespace std;

/* Declaración de la clase Lista_Campeones */

class Individuo
{
    private:
	
	// estructuras privadas
	// cada registro de la lista es una combinación de parametros + fitness	
	int x,y;
	float fitness;
	float beta_cero, beta_forest, beta_aspect, beta_slope, beta_wind;



    public:
      	// constructores
	Individuo();
	
      	// destructor
	~Individuo();

	// funciones publicas
	void imprimirIndividuo(); 

	void set_values(float beta_cero, float beta_forest, float beta_aspect, float beta_slope, float beta_wind);
	void set_fitness(float fitness);
	void set_x(int x);
	void set_y(int y);

	float get_fitness(); 
	float get_beta_cero();
	float get_beta_forest();
	float get_beta_aspect();
	float get_beta_slope();
	float get_beta_wind();

	int get_x();
	int get_y();
};



