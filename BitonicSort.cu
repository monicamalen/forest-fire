/*
 * Parallel bitonic sort using CUDA.
 * Compile with
 * nvcc -arch=sm_11 bitonic_sort.cu
 * Based on http://www.tools-of-computing.com/tc/CS/Sorts/bitonic_sort.htm
 */

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <iostream>

 #include "BitonicSort.h"

/* Every thread gets exactly one value in the unsorted array. */
#define THREADS 512 // 2^9
#define BLOCKS 4 //32768 // 2^15
#define NUM_VALS THREADS*BLOCKS


using namespace std;


/* Auxiliary function headers */
int es_potencia_de_2(int valor);
int encontrar_siguiente_potencia_de_2(int n);
int myrealloc(Fitness_T *d_old, int oldsize, Fitness_T *d_new, int newsize);


void array_print(float *arr, int length) 
{
  int i;
  for (i = 0; i < length; ++i) {
    printf("%1.3f ",  arr[i]);
  }
  printf("\n");
}



__global__ void bitonic_sort_step(Fitness_T *dev_values, int j, int k)
{
  unsigned int i, ixj; /* Sorting partners: i and ixj */
  i = threadIdx.x + blockDim.x * blockIdx.x;
  ixj = i^j;

  /* The threads with the lowest ids sort the array. */
  if ((ixj)>i) {
    if ((i&k)==0) {
      /* Sort ascending */
      if (dev_values[i].fitness > dev_values[ixj].fitness) {
        /* exchange(i,ixj); */
        Fitness_T temp = dev_values[i];
        dev_values[i] = dev_values[ixj];
        dev_values[ixj] = temp;
      }
    }
    if ((i&k)!=0) {
      /* Sort descending */
      if (dev_values[i].fitness < dev_values[ixj].fitness) {
        /* exchange(i,ixj); */
        Fitness_T temp = dev_values[i];
        dev_values[i] = dev_values[ixj];
        dev_values[ixj] = temp;
      }
    }
  }
}



/**
 * Inplace bitonic sort using CUDA.
 */
void bitonic_sort(Fitness_T *d_fitnesses, int size)
{
  
    int nThreads = 16;
    int nBlocks = size / nThreads;

    int j, k;
    /* Major step */
    for (k = 2; k <= size; k <<= 1) {
     /* Minor step */
      for (j=k>>1; j>0; j=j>>1) {
        bitonic_sort_step<<<nBlocks, nThreads>>>(d_fitnesses, j, k);
      }
    }
  


}





/* BITONICSORT TRABAJA CON POTENCIAS DE DOS POR LO QUE HAGO UN PADDING CON VALROES BIEN ALTOS*/
int BitonicSort::Sort(Fitness_T *d_fitnesses)
{

 

  // bitonic sort sirve para potencias de 2
  if (es_potencia_de_2(CANT_INDIVIDUOS)) {
     
      bitonic_sort(d_fitnesses, CANT_INDIVIDUOS); /* Inplace */
  
  }
  else {

      /* calculo nueva dimension el numero proximo potencia de 2 */
      int nueva_dimension_fitness = encontrar_siguiente_potencia_de_2(CANT_INDIVIDUOS);
   

      /* creo un arreglo nuevo y lo lleno */
      Fitness_T *d_auxiliar;
      cudaMalloc((void**)&d_auxiliar, nueva_dimension_fitness*sizeof(Fitness_T));

      /* copio datos y padding en las ultimas posiciones*/
      myrealloc(d_fitnesses, CANT_INDIVIDUOS, d_auxiliar, nueva_dimension_fitness);


      /* ordeno el arreglo con dimension potencia de 2 */
      bitonic_sort(d_auxiliar, nueva_dimension_fitness);

      /* vuelvo poblacino ordenada a d_fitness*/
      myrealloc(d_auxiliar, nueva_dimension_fitness, d_fitnesses, CANT_INDIVIDUOS);

  } 

 
  return 0;

 }



/* Retorna true si valor es potencia de 2 false cc */
 int es_potencia_de_2(int valor) {

  // el metodo dice que hay que pasar valor a binario, y restarle 1 y pasarlo a binario tambien
  // hacer el AND bit a bit y si es 0 es que valor es potencia de 2. 

  int resu = (valor & (valor-1));  // uso operacion binaria & que es un AND binario 

  return (resu==0);
  

 }


int encontrar_siguiente_potencia_de_2(int n){

     unsigned count = 0;

    // el metodo dice que hay que pasar valor a binario, y restarle 1 y pasarlo a binario tambien
    // hacer el AND bit a bit y si es 0 es que valor es potencia de 2. 
       
     /* First n in the below condition is for the case where n is 0*/
     if (n && !(n&(n-1)))
        return n;
 
     while( n != 0)
     {
        n  >>= 1;
        count += 1;
     }
 
    return 1<<count;

}


 __global__ void kernel_rellenar_demas(int oldsize, int newsize, Fitness_T *d_old, Fitness_T *d_newT) {

      int idx = threadIdx.x + (blockIdx.x * blockDim.x);


      /* los primeros oldsize copian el dato anterior, los otros padding 
         con valor alto para que queden atras en el vector */
      Fitness_T aux;

      if (idx < newsize) {

          aux.id = idx;
          aux.fitness = (idx < oldsize) ? d_old[idx].fitness : 999999.9;
      
          d_newT[idx] = aux;
      }
     
 }

 

 __global__ void kernel_rellenar_de_menos(int oldsize, int newsize, Fitness_T *d_old, Fitness_T *d_newT) {


      int idx = threadIdx.x + (blockIdx.x * blockDim.x);
      

      if(idx < newsize) {
          d_newT[idx] = d_old[idx];
      }



 }
   
   

//Fitness_T* myrealloc(int oldsize, int newsize, Fitness_T* old)
int myrealloc(Fitness_T *d_old, int oldsize, Fitness_T *d_new, int newsize)
{
   
    
    int nThreads = 1024;  
    int nBlocks = (newsize / nThreads) +1 ;
    

    if (newsize > oldsize)
      kernel_rellenar_demas<<<nBlocks, nThreads>>>(oldsize, newsize, d_old, d_new);
    else
      kernel_rellenar_de_menos<<<nBlocks, nThreads>>>(oldsize, newsize, d_old, d_new);
      
   
    return 0; 
}


