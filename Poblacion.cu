#include <stdio.h>  /* print */
#include <stdlib.h>
#include <iostream> /* print */
#include "Poblacion.h"
#include "Parameters.h"
#include "BitonicSort.h"
#include "Parameters_Genetico.h"
#include "Genetico.h"
#include <vector>
#include <algorithm>


double box_muller();

using namespace std;


  Poblacion::Poblacion() {

    mi_poblacion = new Individuo[CANT_INDIVIDUOS];
  }


  

  Poblacion::~Poblacion() {


  }  

  /*CASO 2 Parameters.h inicializa la poblacion con valores aleatorios */
  int Poblacion::Inicializar_Poblacion_Random(Individuo_T *h_poblacion, int semilla, vector<int> indices_referencia) {

  	 int i;

	   srandom(semilla);
	   float rnd1,rnd2,rnd3,rnd4,rnd5;
     
  	 for (i = 0; i < CANT_INDIVIDUOS; i++) {

		  	// generamos numeros aleatorios para variar los parametros
	    	rnd1=float(random())/RAND_MAX;
		    rnd2=float(random())/RAND_MAX;
		    rnd3=float(random())/RAND_MAX;
		    rnd4=float(random())/RAND_MAX;
		    rnd5=float(random())/RAND_MAX;
              

        h_poblacion[i].fitness            = 0.0;
	      h_poblacion[i].beta_cero          = (2*rnd1-1)* (float)30.0;
        h_poblacion[i].beta_forest        = (2*rnd2-1)* (float)30.0;
        h_poblacion[i].beta_aspect        = (2*rnd3-1)* (float)30.0;
        h_poblacion[i].beta_slope         = (2*rnd4-1)* (float)30.0;
        h_poblacion[i].beta_wind          = (2*rnd5-1)* (float)30.0;
       

        /*  GENERACION DE x e Y ALEATORIOS */
        if (POBLACION_INICIAL_X_Y_ALEATORIOS) {
     
            int celda;
            // genero nros aleatorios hasta pegarle uno que esté dentro del area quemada (indices_referenica)
            celda = ((int)random() % (ROWS*COLS));

            while  (find(indices_referencia.begin(), indices_referencia.end(), celda) == indices_referencia.end()) { // while no esta!!
                celda = ((int)random() % (ROWS*COLS));
            }
       
            h_poblacion[i].x  = celda % COLS;  
            h_poblacion[i].y  = celda / ROWS;
       
        }
        else {
            
            h_poblacion[i].x  = XIGNI;  
            h_poblacion[i].y  = YIGNI;
       
        } 
    

     } // FOR

  	 return 0;
  }

/*CASO 5 Parameters.h*/    
int Poblacion::Inicializar_Poblacion_Best_Params(Individuo_T *h_poblacion, int semilla, vector<int> indices_fuego_referencia) {


     int i;

     for (i = 0; i < CANT_INDIVIDUOS; i++) {
        
        
        h_poblacion[i].fitness            = 0.0;
        h_poblacion[i].beta_cero          = beta_cero_best ;
        h_poblacion[i].beta_forest        = beta_forest_best;
        h_poblacion[i].beta_aspect        = beta_aspect_best;
        h_poblacion[i].beta_slope         = beta_slope_best;
        h_poblacion[i].beta_wind          = beta_wind_best;
        h_poblacion[i].x                  = XIGNI;
        h_poblacion[i].y                  = YIGNI;


     }

     return 0;


}

/*CASO 4 Parameters.h*/
int Poblacion::Inicializar_Poblacion_Cercana_Best_Params(Individuo_T *h_poblacion) {

     int i;

     srandom(1);
     float rnd1,rnd2,rnd3,rnd4,rnd5;

     for (i = 0; i < CANT_INDIVIDUOS; i++) {

        // generamos numeros aleatorios para variar los parametros
        rnd1=float(random())/RAND_MAX;
        rnd2=float(random())/RAND_MAX;
        rnd3=float(random())/RAND_MAX;
        rnd4=float(random())/RAND_MAX;
        rnd5=float(random())/RAND_MAX;


        
        float beta_cero =   beta_cero_best+ beta_cero_best * (2*rnd1-1)*1; //betas muestreados de una distrib uniforme con centro en los best_params
        float beta_forest = beta_forest_best+beta_forest_best * (2*rnd2-1)*1; 
        float beta_aspect = beta_aspect_best+ beta_aspect_best * (2*rnd3-1)*1; 
        float beta_wind =   beta_wind_best+ beta_wind_best * (2*rnd4-1)*1;  
        float beta_slope =  beta_slope_best+ beta_slope_best * (2*rnd5-1)*1; //masmenos 100porciento de beta

        h_poblacion[i].fitness            = 0.0;
        h_poblacion[i].beta_cero          = beta_cero ;
        h_poblacion[i].beta_forest        = beta_forest;
        h_poblacion[i].beta_aspect        = beta_aspect;
        h_poblacion[i].beta_slope         = beta_slope;
        h_poblacion[i].beta_wind          = beta_wind;
        h_poblacion[i].x                  = XIGNI;
        h_poblacion[i].y                  = YIGNI;


     }

     return 0;
  }

/*CASO 3 en PRIOR at Parameters.h*/
int Poblacion::Inicializar_Poblacion_Gausiana(Individuo_T *h_poblacion,int semilla, vector<int> indices_referencia) {

     int i;

     srandom(semilla);
     
     for (i = 0; i < CANT_INDIVIDUOS; i++) {

        float beta_cero=5*box_muller();
        float beta_forest=5*box_muller();
        float beta_aspect=5*box_muller();
        float beta_wind=5*box_muller();
        float beta_slope=5*box_muller();
               
        h_poblacion[i].fitness            = 0.0;
        h_poblacion[i].beta_cero          = beta_cero ;
        h_poblacion[i].beta_forest        = beta_forest;
        h_poblacion[i].beta_aspect        = beta_aspect;
        h_poblacion[i].beta_slope         = beta_slope;
        h_poblacion[i].beta_wind          = beta_wind;


        /*  GENERACION DE x e Y ALEATORIOS */
        if (POBLACION_INICIAL_X_Y_ALEATORIOS) {
     
            int celda;
            // genero nros aleatorios hasta pegarle uno que esté dentro del area quemada (indices_referenica)
            celda = ((int)random() % (ROWS*COLS));

            while  (find(indices_referencia.begin(), indices_referencia.end(), celda) == indices_referencia.end()) { // while no esta!!
                celda = ((int)random() % (ROWS*COLS));
            }
       
            h_poblacion[i].x  = celda % COLS;  
            h_poblacion[i].y  = celda / ROWS;
       
        }
        else {
            
            h_poblacion[i].x  = XIGNI;  
            h_poblacion[i].y  = YIGNI;
        } 

     }

     return 0;
  }
  

/*CASO 4 en PRIOR at Parameters.h DISTRUBUCION EXPONENCIAL*/
  int Poblacion::Inicializar_Poblacion_Exponencial(Individuo_T *h_poblacion, int semilla, vector<int> indices_referencia) {

  	 int i;

	   srandom(semilla);
	   float rnd1,rnd2,rnd3,rnd4,rnd5;

  	 for (i = 0; i < CANT_INDIVIDUOS; i++) {

		  	// generamos numeros aleatorios para variar los parametros
	    	rnd1=float(random())/RAND_MAX;
		    rnd2=float(random())/RAND_MAX;
		    rnd3=float(random())/RAND_MAX;
		    rnd4=float(random())/RAND_MAX;
		    rnd5=float(random())/RAND_MAX;
     

        h_poblacion[i].fitness            = 0.0;
      	h_poblacion[i].beta_cero          = log( (2*rnd1-1))* (float)10.0;
        h_poblacion[i].beta_forest        = log( (2*rnd2-1))* (float)10.0;
        h_poblacion[i].beta_aspect        = log( (2*rnd3-1))* (float)10.0;
        h_poblacion[i].beta_slope         = log((2*rnd4-1))* (float)10.0;
        h_poblacion[i].beta_wind          = log((2*rnd5-1))* (float)10.0;

        /*  GENERACION DE x e Y ALEATORIOS */
        if (POBLACION_INICIAL_X_Y_ALEATORIOS) {
     
            int celda;
            // genero nros aleatorios hasta pegarle uno que esté dentro del area quemada (indices_referenica)
            celda = ((int)random() % (ROWS*COLS));

            while  (find(indices_referencia.begin(), indices_referencia.end(), celda) == indices_referencia.end()) { // while no esta!!
                celda = ((int)random() % (ROWS*COLS));
            }
       
            h_poblacion[i].x  = celda % COLS;  
            h_poblacion[i].y  = celda / ROWS;
       
        }
        else {
            
            h_poblacion[i].x  = XIGNI;  
            h_poblacion[i].y  = YIGNI;
       
        } 
     }

  	 return 0;
  }

  
  int Poblacion::Imprimir_en_Archivo(char *nombre, Individuo_T *h_poblacion) {

  	int i;
  
 	  FILE * fout;
 	  float v1,v2,v3,v4,v5,v6;
 	  int vx, vy;

   	fout = fopen (nombre, "w+");

   	for (i = 0; i < CANT_INDIVIDUOS; i++) {

   		v1 =	h_poblacion[i].fitness;
   		v2 =	h_poblacion[i].beta_cero;
   		v3 =	h_poblacion[i].beta_forest;
   		v4 =	h_poblacion[i].beta_aspect;
   		v5 =	h_poblacion[i].beta_slope;
   		v6 =	h_poblacion[i].beta_wind;
   		vx = 	h_poblacion[i].x;
   		vy = 	h_poblacion[i].y;
	   
	   	fprintf(fout, "%f %f %f %f %f %f %d %d \n", v1, v2, v3, v4, v5, v6, vx, vy);
    
    }

    fclose(fout);

  	return 0;
  }




  int Poblacion::Imprimir(Individuo_T *h_poblacion) {

    int i;
  
    float v1,v2,v3,v4,v5,v6;
    int vx, vy;

    
    for (i = 0; i < CANT_INDIVIDUOS; i++) {

      v1 =  h_poblacion[i].fitness;
      v2 =  h_poblacion[i].beta_cero;
      v3 =  h_poblacion[i].beta_forest;
      v4 =  h_poblacion[i].beta_aspect;
      v5 =  h_poblacion[i].beta_slope;
      v6 =  h_poblacion[i].beta_wind;
      vx =  h_poblacion[i].x;
      vy =  h_poblacion[i].y;
     
      printf("%f %f %f %f %f %f %d %d \n", v1, v2, v3, v4, v5, v6, vx, vy);
    
    }

   

    return 0;
  }


  

  /* ENVIO LA POBLACION A GPU PARA OPERAR EN LA PLACA */
  int Poblacion::Enviar_Poblacion_a_GPU(Individuo_T *h_poblacion, Individuo_T *d_poblacion) {


      int size = sizeof(Individuo_T) * CANT_INDIVIDUOS;

      cudaMemcpy(d_poblacion, h_poblacion, size, cudaMemcpyHostToDevice);
      if(IMPRIMIR){
      cout << "Poblacion en GPU " << endl;
      }
      return 0;

  }




  int Poblacion::Pasos_evolucion(Individuo_T *d_poblacion, Fitness_T *d_fitnesses, int nro_generacion, thrust::device_vector<int> d_indices_fuego_referencia) {

    Genetico genetico;
  
    genetico.Evolucionar_Poblacion(d_fitnesses, d_poblacion, nro_generacion, d_indices_fuego_referencia);

    return 0;
  }
