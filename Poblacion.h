#pragma once
#include "Parameters_Genetico.h"
#include "Individuo.h"
#include <vector>
#include <thrust/device_vector.h>

/* Declaración de la clase poblacion */
class Poblacion
{
    private:

   

      int dimension;
      int max_size;

      Individuo *mi_poblacion;

     
      
    public:

      
      Poblacion();
      ~Poblacion();

      int Inicializar_Poblacion_Random(Individuo_T *h_poblacion, int semilla, vector<int> indices_fuego_referencia);
      int Inicializar_Poblacion_Best_Params(Individuo_T *h_poblacion, int semilla, vector<int> indices_fuego_referencia);
      int Inicializar_Poblacion_Cercana_Best_Params(Individuo_T *h_poblacion) ;
      int Inicializar_Poblacion_Gausiana(Individuo_T *h_poblacion, int semilla, vector<int> indices_fuego_referencia);
      int Inicializar_Poblacion_Exponencial(Individuo_T *h_poblacion, int semilla, vector<int> indices_fuego_referencia);
      
      
      int Imprimir_en_Archivo(char *nombre ,Individuo_T *h_poblacion);
      int Imprimir(Individuo_T *h_poblacion);
      int Enviar_Poblacion_a_GPU(Individuo_T *h_poblacion, Individuo_T *d_poblacion);
      int Pasos_evolucion(Individuo_T *d_poblacion, Fitness_T *d_fitnesses, int nro_generacion, thrust::device_vector<int> d_indices_fuego_referencia);
	 
};
