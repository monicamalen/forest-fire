#include<iostream>
#include <thrust/host_vector.h>
#include <thrust/device_vector.h>
#include <thrust/device_ptr.h>
#include <thrust/generate.h>
#include <thrust/functional.h>
#include <thrust/iterator/zip_iterator.h>
#include <thrust/transform_reduce.h>

// definir aqui todas las operaciones entre los mapas
struct Interseccion{
   __host__ __device__
   float operator()(thrust::tuple<float,float> tt)	
   {	
	float m1=thrust::get<0>(tt);
	float m2=thrust::get<1>(tt);
	return (m1*m2!=0); // ejemplo de "interseccion"
   }
};
struct Union{
   __host__ __device__
   float operator()(thrust::tuple<float,float> tt)	
   {	
	float m1=thrust::get<0>(tt);
	float m2=thrust::get<1>(tt);
	return (m1!=0 || m2!=0); // ejemplo de "union"
   }
};

// esta es para usar con vectores de thrust
template<typename Type>
float operacion(thrust::device_vector<float> &d_vec1, thrust::device_vector<float> &d_vec2, Type &op)
{
  // todas las operaciones entre los mapas d_vec1, d_vec2, definidas en "operacion()", se pueden hacer asi:
  float resultado=thrust::transform_reduce(
	thrust::make_zip_iterator(thrust::make_tuple(
		d_vec1.begin(),
		d_vec2.begin()
	)),
	thrust::make_zip_iterator(thrust::make_tuple(
		d_vec1.end(),
		d_vec2.end()
	)),
	op,
	0,
	thrust::plus<float>()
  );   
  return resultado; 	
};

// esta es para usar con thrust device ptr. Necesita por eso el size...
template<typename Type>
float operacion(thrust::device_ptr<float> d_vec1, thrust::device_ptr<float> d_vec2, Type &op, int size)
{
  // todas las operaciones entre los mapas d_vec1, d_vec2, definidas en "operacion()", se pueden hacer asi:
  float resultado=thrust::transform_reduce(
	thrust::make_zip_iterator(thrust::make_tuple(
		d_vec1,
		d_vec2
	)),
	thrust::make_zip_iterator(thrust::make_tuple(
		d_vec1+size,
		d_vec2+size
	)),
	op,
	0,
	thrust::plus<float>()
  );   
  return resultado; 	
};
