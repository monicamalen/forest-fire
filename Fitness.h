#pragma once
#include "Parameters.h"

/* Declaración de la clase fitness */
class Fitness
{
    private:
      float fitnessValue;
      int cantI, cantU, cantR;
      float resunion, intersec,arearealquemada;
      float fitness_secuencial(int filas, int cols, float *mapa1, float *mapa2, float *h_vege);
      int calcular_interseccion(float *mapa1, float valor1, float *mapa2, float valor2, int filas, int cols);
      float fitness_metodo_mdenham_secuencial(int filas, int cols, float *mapa1, float *mapa2);
      
      int calcular_celdas_con_valor(float *mapa1, float valor1, int filas, int cols);
      int calcular_union(float *mapa1, float valor1, float *mapa2, float valor2, int filas, int cols);

     
	
    public:

      Fitness();

      float Calcular_Fitness(int filas, int cols, float *mapa1, float *mapa2, float *h_vege);
      float Calcular_Fitness_paralelo(int filas, int cols, float *d_reference_map, float *d_terrain, float *d_vege);
      float Retornar_Fitness();

 	    ~Fitness();
};
