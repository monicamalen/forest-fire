A NVIDIA GPU is necessary for compiling and executing this code (with CUDA installed). 

Makefile uses nvcc compiler. This code is for LINUX OS. 

Two folders are necessary: 

- `mapas_originales`: input raster maps. Raster maps are preformated, all metadata has to be deleted from these files. 
- `mapas_generados`: empty folder, where outputs are stored. 

Parameters.h has to be modified in base on maps format, etc.

Owners:

Karina Laneri, Monica Denham