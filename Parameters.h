#pragma once

#include <stdio.h>
// PARAMETERS

/* RASTER maps rows*/
#ifndef ROWS
#define ROWS 801			/* Terrain's X size*/
#endif

// RASTER maps cols */
#ifndef COLS
#define COLS 801			/* Terrain's Y size*/
#endif


/* Numero total de simulaciones */
#ifndef NTOT_SIMS 
#define NTOT_SIMS 50 
#endif

/* Input maps*/
// MAPS
#ifndef REAL_FIRE_MAP
#define	REAL_FIRE_MAP "mapas_originales/grani_inc.asc"
#endif

#ifndef FINAL_MAP
#define FINAL_MAP "mapas_originales/fuegoFinal.map"
#endif

#ifndef SIMUL_MAP
#define SIMUL_MAP "mapas_originales/fuegoSimulado.map"
#endif

#ifndef SINTETIC_MAP
#define SINTETIC_MAP "mapas_originales/fuegoSintetico.map"
#endif

#ifndef PRUEBA_LECTURA_hterrain
#define PRUEBA_LECTURA_hterrain "mapas_originales/leo_hterrain_prueba.map"
#endif

#ifndef MAX_DIM_CAMPEONES
#define MAX_DIM_CAMPEONES 100
#endif

#ifndef WIND_MAP
#define WIND_MAP "mapas_originales/grani_viento.asc"
#endif

#ifndef ELEV_MAP
#define ELEV_MAP "mapas_originales/grani_alti.asc"
#endif

#ifndef FOREST_MAP
#define FOREST_MAP "mapas_originales/grani_bosque.asc"
#endif

#ifndef SLOPE_MAP
#define SLOPE_MAP "mapas_originales/grani_pend.asc"
#endif

#ifndef ASPECT_MAP
#define ASPECT_MAP "mapas_originales/grani_ori.asc"
#endif

#ifndef ALTI_MAP
#define ALTI_MAP "mapas_originales/grani_alti.asc"
#endif


#ifndef VEGETATION_MAP
#define	VEGETATION_MAP "mapas_originales/grani_vege.asc"
#endif


/* Search algorithm code: 1 for Genetic Algorithm, 0 for rejection-ABC*/
#ifndef CODIGO_ALGORITMO_BUSQUEDA
#define CODIGO_ALGORITMO_BUSQUEDA 1  
#endif


/* CUDA Kernels layout */
#ifndef NTHREADS_X
#define NTHREADS_X 32
#endif

/* CUDA Kernels layout */
#ifndef NTHREADS_Y
#define NTHREADS_Y 32
#endif


#ifndef T_RUN
#define	T_RUN 5000	/* Running time o numero de pasos de simulacion OJO T_RUN / CANT_MAPAS_PELI debe ser entero*/
#endif



/* Constants for printing during simulation. 0 =  no printing */
#ifndef IMPRIMIR_CADA_POBLACION 
#define IMPRIMIR_CADA_POBLACION 0   //1 para imprimir EN PANTALLA los 10 primeros individuos de cada poblacion (los de mejor fitness) 
#endif


#ifndef IMPRIMIR
#define	IMPRIMIR 0		/* 1 para imprimir por pantalla o no imprimir (0) */
#endif

#ifndef HACER_HISTOGRAMA
#define	HACER_HISTOGRAMA 0		/* 1 para hacer el histograma */
#endif


/* For future use: fire data map */
#ifndef MAPA_DATOS_IGN
#define MAPA_DATOS_IGN 0 
#endif


/* Fitness function */ 
#ifndef CODIGO_FUNCION_FITNESS
#define CODIGO_FUNCION_FITNESS 2  //(1=fitness pajaro, 2=fitness Monica con atomic adds, 3=fitness Monica en paralelo con reduce)
#endif


// Para transferir bandera de propagacion del fuego desde device a host las iteraciones iteracion % FUEGO_PROPAGA_NUMERO
#ifndef FUEGO_PROPAGA_NUMERO
#define FUEGO_PROPAGA_NUMERO 1  //
#endif


// ARMAR MAPA DE DATOS DEL AVANCE DEL FUEGO
#ifndef PRINT_MAP
#define PRINT_MAP 0  /*=0 NO imprimo mapas*/
#endif


/* For initial fire map: sinthetic map = 1, real fire map = 0 (from REAL_FIRE_MAP)*/
#ifndef USAR_MAPA_SINTETICO
#define	USAR_MAPA_SINTETICO 1   /*1=usar "mapa sintetico generado con best params" , 0=usar el mapa real del incendio*/
#endif

#ifndef IGNICION_SIMS_DETERMINISTA
#define	IGNICION_SIMS_DETERMINISTA 1	/* Los mapas de las simulaciones tienen punto de ignicion conocido(XIGNI,YIGNI)=1 , ALEATORIO dentro del area quemada de referencia=0*/
#endif

#ifndef IGNICION_REFERENCIA_DETERMINISTA
#define	IGNICION_REFERENCIA_DETERMINISTA 1	/* El mapa de referencia tiene punto de Ignicion conocido(XIGNI,YIGNI)=1, ALEATORIO dentro de TODA la grilla=0 */
#endif

#ifndef XIGNI
#define XIGNI 400  /*Pto de ignicion en X del fuego de referencia valor entre 0 y COLS*/
#endif

#ifndef YIGNI
#define YIGNI 250  /*Pto de ignicion en X del fuego de referencia valor entre 0 y ROWS*/
#endif


#ifndef POBLACION_INICIAL_X_Y_ALEATORIOS
#define POBLACION_INICIAL_X_Y_ALEATORIOS 0  // 0 PARA COMPIAR XIGNI Y IGNI EN LA POBLACION 1 para que sean aleatorios dentro del fuego inicial
#endif



#ifndef DIR_FIRE_PROGRESS
#define DIR_FIRE_PROGRESS "mapas_generados/"
#endif

#ifndef CANT_MAPAS_PELI
#define	CANT_MAPAS_PELI 50		/* si se imprime con PRINT_MAP esta es la cantidad de mapas que componen la peli*/
#endif

#ifndef CANT_MAPAS_REFERENCIA
#define	CANT_MAPAS_REFERENCIA 1		/* promedio sobre PROMEDIOS_REFMAP Mapas iniciales de referencia*/
#endif




#include<stdlib.h>
using namespace std;


// TIEMPOS DE ENCENDIDO DEL FUEGO
#ifndef TIEMPO_MATORRAL
#define TIEMPO_MATORRAL 1  //Cantidad de tiempos de simulacion que queda prendida una celda con matorral
#endif

#ifndef TIEMPO_BOSQUE
#define TIEMPO_BOSQUE 1  //Cantidad de tiempos de simulacion que queda prendida una celda con bosque
#endif

/*Parametros del modelo, los mejores encontrados por Morales et. al. con MCMC*/
#define beta_cero_best -5
//-3.914
#define beta_forest_best -10
//-10.739
#define beta_aspect_best 5
//5.076
#define beta_wind_best 18.1
//18.267
#define beta_slope_best -5

#ifndef DISTRIB_PRIOR
#define DISTRIB_PRIOR 3  //Distribucion de los valores de los parametros de las simulaciones (1=UniformeCentrada, 
//2=UnifRandom,  3=Gausiana, 4=Exponencial, 5= best params sin random)
#endif

