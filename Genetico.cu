/*
 * Parallel bitonic sort using CUDA.
 * Compile with
 * nvcc -arch=sm_11 bitonic_sort.cu
 * Based on http://www.tools-of-computing.com/tc/CS/Sorts/bitonic_sort.htm
 */

#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <sys/time.h>
#include <thrust/device_vector.h>

#include "Genetico.h"
#include "Poblacion.h"
#include "BitonicSort.h" 
#include "Parameters.h"

 // RNG: PHILOX
#include "RNGcommon/Random123/philox.h"
#include "RNGcommon/Random123/u01.h"
typedef r123::Philox2x64 RNG2;
typedef r123::Philox4x64 RNG4;

#define SEED 1332277027LLU   /*semilla para Philox*/

/*Funcion generadora de numeros aleatorios en paralelo*/
__device__
void PhiloxRandomPairGenetico(const unsigned int index, const unsigned int position, float *r1, float *r2);



using namespace std;





 /* HEADERS */
  /* ELITISMO, SELECCION Y CROSSOVER */
  int generar_nueva_poblacion(Fitness_T *d_fitnesses, Individuo_T *d_poblacion, int cantidad_elitismo, Individuo_T *d_nueva_poblacion);

  int mutar_poblacion(Individuo_T *d_poblacion, int cantidad_elitismo, int nro_generacion, thrust::device_vector<int> d_indices_fuego_referencia);




/*  BODIES */
int Genetico::Evolucionar_Poblacion(Fitness_T *d_fitnesses, Individuo_T *d_poblacion, int  nro_generacion,  thrust::device_vector<int> d_indices_fuego_referencia){

    int cantidad_elitismo = (ELITISM) ? (int)((ELITISM_PERCENTAGE * CANT_INDIVIDUOS) / 100) : 0;


    Individuo_T *d_nueva_poblacion;
    cudaMalloc((void**)&d_nueva_poblacion, CANT_INDIVIDUOS * sizeof(Individuo_T));

    /* ELITISMO, SELECCION Y CROSSOVER */
    generar_nueva_poblacion(d_fitnesses, d_poblacion, cantidad_elitismo, d_nueva_poblacion);


    // le mando el nro de la generacion para hacerlo dinamico: primeras generaciones
    // muta dando saltos grandes (para recorrer bien el espacio de busqueda)
    // y a medida que avanzan las generaciones muta con valores cercanods al parametro
    mutar_poblacion(d_nueva_poblacion, cantidad_elitismo, nro_generacion,  d_indices_fuego_referencia);


    // sobreescribir d_poblacion con la nueva generacion
    cudaMemcpy(d_poblacion, d_nueva_poblacion, CANT_INDIVIDUOS * sizeof(Individuo_T), cudaMemcpyDeviceToDevice);

    cudaFree(d_nueva_poblacion);


    return 0;
}


/*  PARA ARRANCAR, ES UN ALGORITMO MUY SIMPLE: TIRO AL AZAR 2 NUMEROS ENTRE 0..CANT_INDIVIDUOS-1. DE
    LOS DOS COPIO EN LA NUEVA POBLACION EL DE MEJOR FITNESS (MINIMO)
    GENERO UNA POBLACION DE CANT_INDIVIDUOS TAMBIEN */

  __global__ void kernel_seleccion_por_torneo(Individuo_T *d_poblacion, Fitness_T *d_fitnesses, Individuo_T *d_nueva_poblacion, int cantidad_elitismo, int semillita, Fitness_T *d_fitnesses_ordenado) {

      int idx = threadIdx.x + (blockIdx.x * blockDim.x);

  
      if (idx < cantidad_elitismo) {
          d_nueva_poblacion[idx] = d_poblacion[d_fitnesses_ordenado[idx].id];
          d_nueva_poblacion[idx].fitness = d_fitnesses_ordenado[idx].fitness;
      }
      else  if ((idx >= cantidad_elitismo) && (idx < CANT_INDIVIDUOS)) {

          float rnd1,rnd2;
          PhiloxRandomPairGenetico(semillita, idx, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.
          
          float fit1, fit2;
          int indice1 = (int)(rnd1 * CANT_INDIVIDUOS);
          int indice2 = (int)(rnd2 * CANT_INDIVIDUOS);

          fit1 = d_fitnesses[indice1].fitness;
          fit2 = d_fitnesses[indice2].fitness;

          
          if (fit1 < fit2)
          {
             d_nueva_poblacion[idx] = d_poblacion[d_fitnesses[indice1].id];
             d_nueva_poblacion[idx].fitness = fit1;
   

          }   
          else {
             d_nueva_poblacion[idx] = d_poblacion[d_fitnesses[indice2].id];
             d_nueva_poblacion[idx].fitness = fit2;
          }

      }

  }



  /* MITAD DE THREADS Y CADA UNO AGARRA 2 INDIVIDUOS, LOS CRUZA Y LOS REESCRIBE */
  __global__ void kernel_crossover(Individuo_T *d_nueva_poblacion, int cantidad_elitismo, int semillita) {

      int idx = threadIdx.x + (blockIdx.x * blockDim.x);


      if ((idx >= cantidad_elitismo) && (idx * 2 + 1 < CANT_INDIVIDUOS)) {
 

          Individuo_T  indi1 = d_nueva_poblacion[idx*2];
          Individuo_T  indi2 = d_nueva_poblacion[idx*2 +1 ];

          // genero un nro random para punto de cruce
          float rnd1,rnd2;
          PhiloxRandomPairGenetico(semillita, idx, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.
   
          /* no hay modulo en CUDA */
          int nro = (rnd1 * 10);
          int resultado_division = (int)(nro / 7); // son 6 parametros se agrega (x,y) como un unico par
          int crosspoint =  nro - (resultado_division * 7 );

      
          int i; 
          float aux;
          int auxCoordenada;

          for (i = crosspoint; i < 6; i++) {
              
              aux = (i == 0) ? indi1.beta_cero : indi2.beta_cero;
              indi1.beta_cero = (i == 0) ? indi2.beta_cero : indi1.beta_cero;
              indi2.beta_cero = (i == 0) ? aux : indi2.beta_cero;

              aux = (i == 1) ? indi1.beta_forest : indi2.beta_forest;
              indi1.beta_forest = (i == 1) ? indi2.beta_forest : indi1.beta_forest;
              indi2.beta_forest = (i == 1) ? aux : indi2.beta_forest;

              aux = (i == 2) ? indi1.beta_aspect : indi2.beta_aspect;
              indi1.beta_aspect = (i == 2) ? indi2.beta_aspect : indi1.beta_aspect;
              indi2.beta_aspect = (i == 2) ? aux : indi2.beta_aspect;

              aux = (i == 3) ? indi1.beta_slope : indi2.beta_slope;
              indi1.beta_slope = (i == 3) ? indi2.beta_slope : indi1.beta_slope;
              indi2.beta_slope = (i == 3) ? aux : indi2.beta_slope;

              aux = (i == 4) ? indi1.beta_wind : indi2.beta_wind;
              indi1.beta_wind = (i == 4) ? indi2.beta_wind : indi1.beta_wind;
              indi2.beta_wind = (i == 4) ? aux : indi2.beta_wind;


              /* tomo (x,y) como un par, los cambio a los dos (por eso tomo 6 parametros y no 7 y pregunto 2 veces por 5!!) */
              auxCoordenada = (i == 5) ? indi1.x : indi2.x;
              indi1.x = (i == 5) ? indi2.x : indi1.x;
              indi2.x = (i == 5) ? auxCoordenada : indi2.x;

              /* lo mismo con y */
              auxCoordenada = (i == 5) ? indi1.y : indi2.y;
              indi1.y = (i == 5) ? indi2.y : indi1.y;
              indi2.y = (i == 5) ? auxCoordenada : indi2.y;

          }

          indi1.fitness = 0.0f;
          indi2.fitness = 0.0f;
          d_nueva_poblacion[idx*2] = indi1;
          d_nueva_poblacion[idx*2+1] = indi2;
      }     



  }



/* ELITISMO, SELECCION Y CROSSOVER */
  int generar_nueva_poblacion(Fitness_T *d_fitnesses, Individuo_T *d_poblacion, int cantidad_elitismo, Individuo_T *d_nueva_poblacion){

    // instancio tantos threads como individuos tengo, cada thread elige 2 individuos y se escribe 
    // en la nueva poblacion el de la nueva poblacion. 

      int nThreads, nBlocks;


      int semillita = time(NULL);

      // elitismo, declaro el arreglo
      Fitness_T *d_fitnesses_ordenado;
      cudaMalloc((void**)&d_fitnesses_ordenado, sizeof(Fitness_T) * CANT_INDIVIDUOS);

      // si hay elitismo tengo que enviar la poblacion (el arreglo fitness) ordenado para elegir los cantidad_elitismo primeros
      if (cantidad_elitismo > 0) 
      {
          BitonicSort orden;

        // copio el arreglo
          cudaMemcpy(d_fitnesses_ordenado, d_fitnesses, sizeof(Fitness_T) * CANT_INDIVIDUOS, cudaMemcpyDeviceToDevice);
          orden.Sort(d_fitnesses_ordenado);
      }


      if (CANT_INDIVIDUOS <=1024 ) {
          nThreads = CANT_INDIVIDUOS;
          nBlocks = 1;
      }
      else {
          nThreads = 1024;
          nBlocks = CANT_INDIVIDUOS % nThreads?  CANT_INDIVIDUOS / nThreads + 1 : CANT_INDIVIDUOS / nThreads;
      }

      /* Lanzo 1 thread por individuo de la poblacion*/
      /* bien simple: tiro 2 nros aleatorios y los uso para elegir 2 indivuos. Escribo el de fitness menor */
      kernel_seleccion_por_torneo<<<nBlocks, nThreads>>>(d_poblacion, d_fitnesses, d_nueva_poblacion, cantidad_elitismo, semillita, d_fitnesses_ordenado);
      cudaDeviceSynchronize();


      /* Lanzo la mitad de threads de numero de invidivuos de la poblacion */
      nThreads = nThreads / 2;
      kernel_crossover<<<nBlocks, nThreads>>>(d_nueva_poblacion, cantidad_elitismo, semillita);
      cudaDeviceSynchronize();
      

      cudaFree(d_fitnesses_ordenado);

      return 0;
  }

  



/* se lanza un thread por individuo */ 
__global__ void kernel_mutacion(Individuo_T *d_poblacion, int cantidad_elitismo, int semillita, int nro, int *puntero_indices, int indices_fuego_referencia_size) {


      int idx = threadIdx.x + (blockIdx.x * blockDim.x);

      if ((idx >= cantidad_elitismo) && (idx < CANT_INDIVIDUOS)) {

        Individuo_T individuo_aux;

        individuo_aux = d_poblacion[idx]; 

        // genero un nro random para punto de cruce
        float rnd1,rnd2;

        
    
        PhiloxRandomPairGenetico(semillita, idx*2, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.
        individuo_aux.beta_cero  = individuo_aux.beta_cero + ((rnd1 < MUTATION_PROBABILITY) ? ((2*rnd2-1)*10.0) * (float)nro : 0.0 );

        PhiloxRandomPairGenetico(semillita, idx*3, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.
        individuo_aux.beta_forest = individuo_aux.beta_forest + ((rnd1 < MUTATION_PROBABILITY) ? ((2*rnd2-1)*10.0) * (float)nro : 0.0 );

        PhiloxRandomPairGenetico(semillita, idx*4, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.
        individuo_aux.beta_aspect = individuo_aux.beta_aspect + ((rnd1 < MUTATION_PROBABILITY) ? ((2*rnd2-1)*10.0) * (float)nro : 0.0 );

        PhiloxRandomPairGenetico(semillita, idx*5, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.
        individuo_aux.beta_slope  = individuo_aux.beta_slope + ((rnd1 < MUTATION_PROBABILITY) ? ((2*rnd2-1)*10.0) * (float)nro : 0.0 );

        PhiloxRandomPairGenetico(semillita, idx*6, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.
        individuo_aux.beta_wind   = individuo_aux.beta_wind + ((rnd1 < MUTATION_PROBABILITY) ? ((2*rnd2-1)*10.0) * (float)nro : 0.0) ;


        // este if no genera divergencia porque para todos los threads es igual
        if (X_Y_MUTANTES) {

            PhiloxRandomPairGenetico(semillita, idx, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.
      
            /* calculo un indice dentro del vector por si hay que mutar */
            int aux1 = ((int)(rnd2 * 10 * indices_fuego_referencia_size )) % indices_fuego_referencia_size; // hago que 

            // si hay que mutar saco una celda del arreglo y lo convierto en fila,columna, sino dejo lo que habia. 
            individuo_aux.x = (rnd1 < MUTATION_PROBABILITY) ? puntero_indices[aux1] % COLS : individuo_aux.x;  
            individuo_aux.y = (rnd1 < MUTATION_PROBABILITY) ? puntero_indices[aux1] / ROWS : individuo_aux.y;

        }

        d_poblacion[idx] = individuo_aux;


      }
}


  int mutar_poblacion(Individuo_T *d_poblacion, int cantidad_elitismo, int nro_generacion,  thrust::device_vector<int> d_indices_fuego_referencia) {

      int nThreads, nBlocks;

      int semillita = time(NULL);

     
      if (CANT_INDIVIDUOS <=1024 ) {
          nThreads = CANT_INDIVIDUOS;
          nBlocks = 1;
      }
      else {
          nThreads = 1024;
          nBlocks = CANT_INDIVIDUOS % nThreads?  CANT_INDIVIDUOS / nThreads + 1 : CANT_INDIVIDUOS / nThreads;
      }


      // quiero hacer una mutacion dinamica!!! que a medida que avanzo en las generaciones vaya disminuyendo el rango de mutacion
    int nro = (CANT_EVOLUCIONES / CANT_RANGOS_MUTACION_DINAMICA) + (CANT_EVOLUCIONES % CANT_RANGOS_MUTACION_DINAMICA? 1 : 0 );
    int nro_dos = (int) (nro_generacion / nro);
    int nro_tres = pow(10,nro_dos);
    float resu = 1.0f/(float)nro_tres;
 
    /* Lanzo 1 thread por individuo de la poblacion, no puedo usar thrust en kernels: mando puntero crudo del container y su longitud */
    int indices_fuego_referencia_size = d_indices_fuego_referencia.size();
    int *puntero_indices = thrust::raw_pointer_cast(&d_indices_fuego_referencia[0]);
    //float * x_ptr = thrust::raw_pointer_cast(&x[0]);
    
    // paso el container d_indices_fuego_referencia

    kernel_mutacion<<<nBlocks, nThreads>>>(d_poblacion, cantidad_elitismo, semillita, resu, puntero_indices, indices_fuego_referencia_size);
    cudaDeviceSynchronize();
    

     return 0;
  }





/*Funcion generadora de numeros aleatorios en paralelo*/
__device__
void PhiloxRandomPairGenetico(const unsigned int index, const unsigned int position, float *r1, float *r2)
{
//index pasar un tiempo i de T_RUN (para no generar correlaciones en el t)
//position pasar el indice lineal de la posicion (cada posicion a cadat tiene un num aleatorio distinto)
  RNG2 rng;
  RNG2::ctr_type c_pair={{}};
  RNG2::key_type k_pair={{}};
  RNG2::ctr_type r_pair;

    // keys = threadid
    k_pair[0]= index;
    // time counter
    c_pair[0]= position;
    c_pair[1]= SEED; // semilla general definida en el main
    // random number generation
    r_pair = rng(c_pair, k_pair);

    *r1= u01_closed_closed_64_53(r_pair[0]);  //un mum entre 0 y 1
    *r2= u01_closed_closed_64_53(r_pair[1]);  //otro num entre 0 y 1
}
