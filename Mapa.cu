//#pragma once
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <vector>
#include <math.h>   /*math functions stl library*/
#include <algorithm>  /* std::min_element, std::max_element*/

#include "Parameters.h"
#include "Mapa.h"

#include <thrust/swap.h>
#include <thrust/device_vector.h>
#include <thrust/host_vector.h>
#include "curso.h"

#define gpuErrchk(ans) { gpuAssert((ans), __FILE__, __LINE__); }
inline void gpuAssert(cudaError_t code, char *file, int line, bool abort=true)
{
   if (code != cudaSuccess) 
   {
      fprintf(stderr,"GPUassert: %s %s %d\n", cudaGetErrorString(code), file, line);
      if (abort) exit(code);
   }
}



#define MAX 100

#define WITHIN_BOUNDS( x ) ( x > -1 ) // por como está la función get_index, si no esta entre filas y cols retrona -1

// RNG: PHILOX
#include "RNGcommon/Random123/philox.h"
#include "RNGcommon/Random123/u01.h"
typedef r123::Philox2x64 RNG2;
typedef r123::Philox4x64 RNG4;

#define SEED 1332277027LLU   /*semilla para Philox*/

/*Funcion generadora de numeros aleatorios en paralelo*/
__device__
void PhiloxRandomPair(const unsigned int index, const unsigned int position, float *r1, float *r2)
{
//index pasar un tiempo i de T_RUN (para no generar correlaciones en el t)
//position pasar el indice lineal de la posicion (cada posicion a cadat tiene un num aleatorio distinto)
	RNG2 rng;
	RNG2::ctr_type c_pair={{}};
	RNG2::key_type k_pair={{}};
	RNG2::ctr_type r_pair;

		// keys = threadid
		k_pair[0]= index;
		// time counter
		c_pair[0]= position;
		c_pair[1]= SEED; // semilla general definida en el main
		// random number generation
		r_pair = rng(c_pair, k_pair);

		*r1= u01_closed_closed_64_53(r_pair[0]);  //un mum entre 0 y 1
		*r2= u01_closed_closed_64_53(r_pair[1]);  //otro num entre 0 y 1
}






__device__ int get_index(int fila, int col, int filas, int cols){
    // indice en 1 dimension para el elemento (fila,col) en un arreglo bidimensional de (filas x cols) 
    int index=-1;
    if ( fila < filas && col < cols){
        index = fila*cols + col;
    }	
    return index;
}


/* calcula la pendiente en base a los mapas de pendiente y de altitud y como lo explica el paper  */
__device__ 
float calculate_sigma_value(float *d_slope, float *d_alti, int target_index, int neighbor_index)
{
	float slope = 0.0;

	if (WITHIN_BOUNDS(neighbor_index)) {

		float neighbor_slope = (float) d_slope[neighbor_index];

	    slope = ((d_alti[neighbor_index] <= d_alti[target_index]) || (neighbor_slope <= 0) ) ? 0.0 : 1;	 

	}
	
	return slope;
}


/* calculo del valor psi como esta en el paper Modificar para que lo calcule 1 sola vez*/
__global__ 
void KERNEL_calculate_psi_value(float *d_aspect, float *d_slope, int filas, int cols)
{
	int fila = (blockIdx.y * blockDim.y) + threadIdx.y ;
	int col =  (blockIdx.x * blockDim.x) + threadIdx.x ;

	if ((fila < filas) && (col < cols))
	{

		d_aspect[fila * cols + col] = (d_slope[fila * cols + col] > 5) ? cos(d_aspect[fila * cols + col] - 365.0) : 0.0;

	}
}



__device__
int calculate_angle_between_cells(int target_index, int neighbor_index, int filas, int cols)
{
	// lo calculo como el eje cartesiano
	int angulo =  -1;

	if (WITHIN_BOUNDS(target_index) && WITHIN_BOUNDS(target_index))	{
		int target_fila, target_col;
		int neighbor_fila, neighbor_col;

		int matriz[3][3] = {{315,0,45}, {270,-1,90}, {225,180,135}};  // segun explicacion de mm: 0 es el N. clockwise

		target_fila = target_index / cols;
		target_col = target_index % cols;

		neighbor_fila = neighbor_index / cols;
		neighbor_col = neighbor_index % cols;

		int diff_x = (target_col - neighbor_col) * -1 ; // multiplico por menos 1 porque es T-N y si N está a la izq necesito -1 y me queda 1 y al revés
		int diff_y = (target_fila - neighbor_fila) * -1 ; // lo mismo que para las filas 

		angulo = matriz[diff_y+1][diff_x+1];	// para pasar los rangos -1..1 a 0..2 que son los indices de la matriz
	}

	return angulo;
}

/* calculo de omega segun paper omega = cos (fi_w - fi_c) que es la diferencia entre el angulo de la direccion del viento y el angulo formado por celda target y celda neighbor*/
__device__
float calculate_omega_value(float *d_wind, int target_index, int neighbor_index, int filas, int cols)
{
	float wind_dir = 0.0;

	if (WITHIN_BOUNDS(target_index))	{

		float fi_w = (d_wind[target_index] >= 180) ? (d_wind[target_index] - 180) : (d_wind[target_index] + 180) ;  // direccion del viento, ver si es esto o d_wind - 180
		float fi_c = calculate_angle_between_cells(target_index, neighbor_index, filas, cols);
		wind_dir = cos(fi_w - fi_c) ;

	}
	return wind_dir;
}


/*Ignition probability of the target cell*/
__device__ float IgnitionProbability(float * d_mapa, float * d_forest, float * d_wind, float * d_aspect, float * d_slope, float *d_alti, int target_index,int neighbor_index, 
					      int filas, int cols,float beta_cero, float beta_forest,float beta_aspect,float beta_slope, float beta_wind)

{
    /*Ignition Probability from manuscript Morales, Mermoz, Kitzberger*/
     float prob_ignition=1.0 /(1 + exp(-(beta_cero +			// baseline ignition probability for matorral		
 		    beta_forest   * d_forest[target_index] +													// forest
 		    beta_wind     * calculate_omega_value(d_wind, target_index, neighbor_index, filas, cols) +	// wind
 		    beta_aspect   * d_aspect[target_index] +		    // aspect ya transformado por direccion,humedad,pendiente
		    beta_slope    * calculate_sigma_value(d_slope, d_alti, target_index, neighbor_index) 		// slope
  		    ))) ;
		    
     return prob_ignition;
}


__global__ 
void KERNEL_MapaProbaMaxIgnition(float * d_pmap, float * d_mapa, float * d_vege, float * d_forest, float * d_wind, float * d_aspect,
					  float * d_slope, float *d_alti, int filas, int cols,float beta_cero, 
					  float beta_forest,float beta_aspect,float beta_slope, float beta_wind, DatosCelda * d_mapa_datos)
{
	int fila = (blockIdx.y * blockDim.y) + threadIdx.y ;
	int col =  (blockIdx.x * blockDim.x) + threadIdx.x ;
	
	/* Conversion coordenadas a su indice en un arreglo unidimensional */
	int index = get_index(fila, col, filas,cols);

	/* Test para ver si la coordenada del thread cae dentro de las celdas que actualizamos */
	int within_bounds = (fila < filas-1) && (col < cols-1) && (fila > 0) && (col>0);
	if (within_bounds)
	{
		int x=fila;
		int y=col;

		// reduzco las llamadas a get_index, lo calculo 1 vez y paso esto como parametro
		int target_index = get_index(x,y,filas,cols);
		int neighbor_index;
		//miro a mis 8 vecinos si no esta quemado 
		float proba_noignition=1.0; 
	  	for (int f = -1; f <= 1; f++)
	  	{
	   		for(int c = -1; c <= 1; c++) 
	    		{
	      			neighbor_index = get_index(x+c, y+f, filas, cols);
	      			proba_noignition = proba_noignition*(1.0-IgnitionProbability(d_mapa, d_forest, d_wind, d_aspect, d_slope, d_alti, target_index, neighbor_index, filas, cols, beta_cero,  
		      										 beta_forest, beta_aspect, beta_slope, beta_wind));
	    		}
	  	}
	   	d_pmap[target_index]=1.0-proba_noignition;
	}					
        
}




/*se quema o no se quema la celda (x,y) en donde estoy parada?*/
__global__ void KERNEL_Fire_Propagation_Referencia(float * d_mapa, float * d_vege, float * d_forest, float * d_wind, float * d_aspect,float * d_slope, float *d_alti, int filas, int cols,int semilla, float * d_mapanew,
		float beta_cero, float beta_forest, float beta_aspect, float beta_slope, float beta_wind, DatosCelda * d_mapa_datos)
{
	/*un thread para cada celda*/
	int fila = (blockIdx.y * blockDim.y) + threadIdx.y ;
	int col =  (blockIdx.x * blockDim.x) + threadIdx.x ;



	/* Conversion coordenadas a su indice en un arreglo unidimensional */
	int index = get_index(fila, col, filas,cols);

	
	/* Test para ver si la coordenada del thread cae dentro de las celdas que actualizamos */
	int within_bounds = (fila < filas-1) && (col < cols-1) && (fila > 0) && (col>0);
	if (within_bounds)
	{
		int x=fila;
		int y=col;


		// reduzco las llamadas a get_index, lo calculo 1 vez y paso esto como parametro
		int target_index = get_index(x,y,filas,cols);
		int neighbor_index;
		//miro a mis 8 vecinos si no esta quemado 
		float prob_no_ignition=1.0; 
		int DuracionFuego; //Duracion del fuego en pasos, definida segun tipo de vegetacion en Parameters.h
		int tiempoEncendida= d_mapa_datos[get_index(x, y, filas,cols)].timeFireOn; //acumulo el tiempo que dura encendida una celda
		//Defino los tiempos de apagado de celdas sgun el tipo de vegetacion

		// obtengo la vegetacion que hay en la celda target
		float target_vege = d_vege[target_index];


		DuracionFuego = (target_vege == 1.0) ? TIEMPO_MATORRAL : ((target_vege == 2.0) ? TIEMPO_BOSQUE : 0.0);		
		// solo proceso celdas NO quemadas
		if(target_vege != 0.0) //la celda es combustible
		{
			if(d_mapa[target_index]==0.0) //la celda no esta quemada
			{
				int f,c;

				/* con los for de -1 a 1 reviso las 8 celdas que rodean a la celda target. Cubro todas las combinaciones de +1 y -1 */
				for (f = -1; f <= 1; f++) 
					for(c = -1; c <= 1; c++) 
						if (!(f==0 && c== 0))  // si f==0 && c==0 es que estoy en la celda que analizo, no en una vecina
						{	
							neighbor_index = get_index(x+c, y+f, filas, cols);
						    if(d_mapa[neighbor_index]==1.0)
							{
								// calculo index_target e index_neighbor una sola vez y paso como parametro
								
								prob_no_ignition = prob_no_ignition *(1.0-IgnitionProbability(d_mapa, d_forest, d_wind, d_aspect, 
										d_slope, d_alti, target_index, neighbor_index, filas, cols, beta_cero,  beta_forest, beta_aspect, beta_slope, beta_wind)); 
								//acumulo la probabilidad de que no se queme la celda en la que estoy		
							}
						}	

				float prob_total_ignition =1.0-prob_no_ignition; //proba que se queme=1-la proba de que no se queme
			
			

				if(prob_total_ignition==0.0)  d_mapanew[target_index]= 0.0; //no quema
				else
				{  
					float rnd1,rnd2;
					PhiloxRandomPair(semilla, index, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.

					if(rnd1 < prob_total_ignition){
						// SE QUEMA LA CELDA HAGO TODO LO QUE TENGO QUE HACER
						d_mapanew[target_index]= 1.0;
					

						d_mapa_datos[target_index].timeFireOn++;  // si se quema
					}     
				}
			}  
			else {  // celda ya estaba quemada veo si la apago o no segun el tipo de vegetacion
			d_mapanew[target_index] = ( tiempoEncendida > DuracionFuego) ? -1.0 : 1.0;

			d_mapa_datos[target_index].timeFireOn += (tiempoEncendida <= DuracionFuego) ? 1 : 0;		
			}
		}
		else {
			d_mapanew[target_index]=0.0; //la celda seguira siendo no combustible
		}
	}
	else // not within bounds
		d_mapanew[index] = 0.0;  // pongo 0s en los bordes??
}





/*se quema o no se quema la celda (x,y) en donde estoy parada?*/
__global__ void KERNEL_Fire_Propagation(float * d_mapa, float * d_vege, float * d_forest, float * d_wind, float * d_aspect,float * d_slope, float *d_alti, int filas, int cols,int semilla, float * d_mapanew,
		Individuo_T *d_poblacion, int indice_individuo, DatosCelda * d_mapa_datos, int *d_fuego_propaga, int verificar_propagacion)
{
	/*un thread para cada celda*/
	int fila = (blockIdx.y * blockDim.y) + threadIdx.y ;
	int col =  (blockIdx.x * blockDim.x) + threadIdx.x ;

	/* Conversion coordenadas a su indice en un arreglo unidimensional */
	int index = get_index(fila, col, filas,cols);
	
	/* Test para ver si la coordenada del thread cae dentro de las celdas que actualizamos */
	int within_bounds = (fila < filas-1) && (col < cols-1) && (fila > 0) && (col>0);

	if (within_bounds)
	{
		int x=fila;
		int y=col;

		/* acceso solo una vez a memoria global para obtener el individuo */
		Individuo_T  individuo_aux;
		individuo_aux = d_poblacion[indice_individuo];
		
	/* accedo a memoria global para obtener los valores. Ver si esto lo paso a memoria compartida*/
		float beta_cero 	= individuo_aux.beta_cero ;
		float beta_forest 	= individuo_aux.beta_forest;
		float beta_aspect 	= individuo_aux.beta_aspect;
		float beta_slope 	= individuo_aux.beta_slope;
		float beta_wind 	= individuo_aux.beta_wind;

		
		// reduzco las llamadas a get_index, lo calculo 1 vez y paso esto como parametro
		int target_index = get_index(x,y,filas,cols);
		int neighbor_index;
		//miro a mis 8 vecinos si no esta quemado 
		float prob_no_ignition=1.0; 
		int DuracionFuego; //Duracion del fuego en pasos, definida segun tipo de vegetacion en Parameters.h
		int tiempoEncendida= d_mapa_datos[get_index(x, y, filas,cols)].timeFireOn; //acumulo el tiempo que dura encendida una celda
		//Defino los tiempos de apagado de celdas sgun el tipo de vegetacion

		// obtengo la vegetacion que hay en la celda target
		float target_vege = d_vege[target_index];


		DuracionFuego = (target_vege == 1.0) ? TIEMPO_MATORRAL : ((target_vege == 2.0) ? TIEMPO_BOSQUE : 0);	
				
		// solo proceso celdas NO quemadas
		if(target_vege != 0.0) //la celda es combustible
		{
			if(d_mapa[target_index]==0.0) //la celda no esta quemada
			{
				int f,c;

				/* con los for de -1 a 1 reviso las 8 celdas que rodean a la celda target. Cubro todas las combinaciones de +1 y -1 */
				for (f = -1; f <= 1; f++) 
					for(c = -1; c <= 1; c++) 
						if (!(f==0 && c== 0))  // si f==0 && c==0 es que estoy en la celda que analizo, no en una vecina
						{	
							neighbor_index = get_index(x+c, y+f, filas, cols);
						    if(d_mapa[neighbor_index] == 1.0)
							{
								prob_no_ignition = prob_no_ignition *(1.0-IgnitionProbability(d_mapa, d_forest, d_wind, d_aspect, 
										d_slope, d_alti, target_index, neighbor_index, filas, cols, beta_cero,  beta_forest, beta_aspect, beta_slope, beta_wind)); 
								//acumulo la probabilidad de que no se queme la celda en la que estoy		
							}
						}	

				//if(prob_no_ignition==0)printf("proba no ingnic: %f \n", prob_no_ignition);	
				float prob_total_ignition = 1.0 - prob_no_ignition; //proba que se queme=1-la proba de que no se queme
			
			
				if(prob_total_ignition == 0.0)  d_mapanew[target_index]= 0.0; //no quema
				else
				{  
					float rnd1,rnd2;
					PhiloxRandomPair(semilla, index, &rnd1, &rnd2); //tira dos numeron aleatorios e/0y1.

					if(rnd1 < prob_total_ignition){
						// SE QUEMA LA CELDA HAGO TODO LO QUE TENGO QUE HACER
						d_mapanew[target_index]= 1.0;
						
								
						if ((verificar_propagacion) && (!d_fuego_propaga[0])) {
							atomicAdd(&d_fuego_propaga[0],1);
						
						}

						d_mapa_datos[target_index].timeFireOn++;  // si se quema
					}     
				}
			}  
			else {  // celda ya estaba quemada veo si la apago o no segun el tipo de vegetacion
					
				d_mapanew[target_index] = ( tiempoEncendida > DuracionFuego) ? -1.0 : 1.0;
				
				d_mapa_datos[target_index].timeFireOn += (tiempoEncendida <= DuracionFuego) ? 1 : 0;
					     
			}
		}
		else {
			d_mapanew[target_index]=0.0; //la celda seguira siendo no combustible
		}
	}else
	d_mapanew[index] = 0.0;  // pongo 0s en los bordes??
}




void Mapa::Fire_Propagation_Fuego_de_Referencia(int semilla, Individuo_T individuo)
{


	dim3 dimBlock(16, 16);
	dim3 dimGrid((ROWS+dimBlock.x-1) / dimBlock.x, (COLS + dimBlock.y - 1) / dimBlock.y);


  	float beta_cero 	= individuo.beta_cero;
  	float beta_forest	= individuo.beta_forest;
  	float beta_aspect	= individuo.beta_aspect;
  	float beta_slope	= individuo.beta_slope;
  	float beta_wind		= individuo.beta_wind;


	/* envio puntero a la poblacion e indice del individuo */
	KERNEL_Fire_Propagation_Referencia<<<dimGrid, dimBlock>>>(d_terrain,d_vege, d_forest,d_wind,d_aspect,d_slope, d_alti, ROWS,COLS,semilla,d_terrainnew,
						       beta_cero, beta_forest, beta_aspect, beta_slope, beta_wind, d_mapa_datos );


	
	checkCUDAError("Launch faiulre:3 ");  
	cudaDeviceSynchronize(); 
	
	//wrap raw pointers d_terrain and d_terrainnew to use with thrust
	thrust::device_ptr<float> d_terrainnew_ptr(d_terrainnew); 
	thrust::device_ptr<float> d_terrain_ptr(d_terrain);

	try{
	    thrust::swap_ranges(d_terrainnew_ptr,d_terrainnew_ptr+(ROWS*COLS),d_terrain_ptr);
	}  
	catch(thrust::system_error &e){
	  std::cout << "error calculando swap: " << e.what() << std::endl; 	  
	}
	
};






void Mapa::Fire_Propagation(int semilla, int *h_fuego_propaga, int *d_fuego_propaga, int verificar_propagacion, Individuo_T *d_poblacion, int indice_individuo)
{

	dim3 dimBlock(16,16);
	dim3 dimGrid((ROWS + dimBlock.x-1) / dimBlock.x, (COLS + dimBlock.y -1) / dimBlock.y,1);
	
  	if (verificar_propagacion) {
 		h_fuego_propaga[0] = 0;  // pongo la bandera en 0 para que alguno la cambie si propaga sino queda en 0!
		HANDLE_ERROR(cudaMemcpy(d_fuego_propaga, h_fuego_propaga, sizeof(int), cudaMemcpyHostToDevice));
	}

	/* envio puntero a la poblacion e indice del individuo */
	KERNEL_Fire_Propagation<<<dimGrid, dimBlock>>>(d_terrain,d_vege, d_forest,d_wind,d_aspect,d_slope, d_alti, ROWS,COLS,semilla,d_terrainnew,
						       d_poblacion, indice_individuo, d_mapa_datos, d_fuego_propaga, verificar_propagacion );


	checkCUDAError("Launch failure: 1 ");

	cudaDeviceSynchronize(); 

	if (verificar_propagacion)
		HANDLE_ERROR(cudaMemcpy(h_fuego_propaga, d_fuego_propaga, sizeof(int), cudaMemcpyDeviceToHost));

	  //	cout << "Dentro de Fire_Propagation h_fuego_propaga: " << h_fuego_propaga[0] << endl;
	
	
	//wrap raw pointers d_terrain and d_terrainnew to use with thrust
	thrust::device_ptr<float> d_terrainnew_ptr(d_terrainnew); 
	thrust::device_ptr<float> d_terrain_ptr(d_terrain);

	try{
	    thrust::swap_ranges(d_terrainnew_ptr,d_terrainnew_ptr+(ROWS*COLS),d_terrain_ptr);
	}  
	catch(thrust::system_error &e){
	  std::cout << "error calculando swap: " << e.what() << std::endl; 	  
	}
	
};


// Destructor
Mapa::~Mapa()
{
    cudaFree(d_terrain);
    cudaFree(d_vege);
    cudaFree(d_terrainnew);
    cudaFree(d_wind);
    cudaFree(d_forest);
    cudaFree(d_slope);
    cudaFree(d_alti);
    cudaFree(d_real_fire);
    cudaFree(d_aspect);
    cudaFree(d_reference_map);  // este es uno solo en device
    cudaFree(d_pmap);
    cudaFree(d_mapa_datos);
    cudaFree(d_fuego_propaga);


    free(h_terrain);
    free(h_vege);
    free(h_aspect);
    free(h_slope);
    free(h_wind);
    free(h_forest);
    free(h_alti);
    free(h_pmap);
    free(h_mapa_datos);
    free(h_fuego_propaga); 


	// itero sobre todos los mapas de referencia
	for(int nref=0; nref < CANT_MAPAS_REFERENCIA;nref++) 
	{
	  free(h_reference_map[nref]);
	}
	free(h_reference_map);
}


// constructor vacio
Mapa::Mapa()
{

}

Mapa::Mapa(int filas_, int cols_)
{
	filas=filas_;
	cols=cols_;
	 // mapas en host
	h_terrain = (float *)malloc(sizeof(float) * filas * cols );
	h_wind = (float*)malloc(sizeof(float) * filas * cols);
	h_aspect = (float *)malloc(sizeof(float) * filas * cols );
	h_slope = (float*) malloc(sizeof(float) * filas * cols);
	h_forest = (float *)malloc(sizeof(float) * filas * cols );   // ojo que hay un mapa "bosque" y un mapa "vegetacion"
	h_alti = (float *)malloc(sizeof(float) * filas * cols );
	h_vege = (float *)malloc(sizeof(float) * filas * cols );

	h_reference_map= (float**)malloc(sizeof(float*)*CANT_MAPAS_REFERENCIA);
	for(int nref=0; nref<CANT_MAPAS_REFERENCIA;nref++) //voy a promediar sobre CANT_MAPAS_REFERENCIA mapas iniciales
	{
	  if (( h_reference_map[nref] = (float*)malloc(sizeof(float)*ROWS*COLS)) == NULL) {
	    printf("No aloca %d \n", nref);
	    exit (-1);
	  }
	}
	
	h_pmap = (float *)malloc(sizeof(float) * filas * cols );
	
	// mapas en device
	HANDLE_ERROR(cudaMalloc((void**)&d_terrain, sizeof(float) * filas * cols));
	HANDLE_ERROR(cudaMalloc((void**)&d_wind, sizeof(float) * filas * cols));
	HANDLE_ERROR(cudaMalloc((void**)&d_aspect, sizeof(float) * filas * cols));
	HANDLE_ERROR(cudaMalloc((void**)&d_slope, sizeof(float) * filas * cols));
	HANDLE_ERROR(cudaMalloc((void**)&d_forest, sizeof(float) * filas * cols));
	HANDLE_ERROR(cudaMalloc((void**)&d_alti, sizeof(float) * filas * cols));
	HANDLE_ERROR(cudaMalloc((void**)&d_vege, sizeof(float) * filas * cols));
	HANDLE_ERROR(cudaMalloc((void**)&d_reference_map, sizeof(float) * filas * cols));
	HANDLE_ERROR(cudaMalloc((void**)&d_pmap, sizeof(float) * filas * cols)); //mapa de probabilidades
	
	// solo en device, hace de mapa auxiliar para la actualización
	HANDLE_ERROR(cudaMalloc((void**)&d_terrainnew, sizeof(float) * filas * cols));
	
	// PENDIENTES A VER SI SE USAN
	h_mapa_datos = (DatosCelda*) malloc(sizeof(DatosCelda) * filas * cols);
	HANDLE_ERROR(cudaMalloc((void**)&d_mapa_datos, sizeof(DatosCelda) * filas * cols));		
	

	/* Agrego aca el FLAG de fuego_propaga*/
	h_fuego_propaga = (int*)malloc(sizeof(int));
	HANDLE_ERROR(cudaMalloc((void**)&d_fuego_propaga, sizeof(int)));

	if (!h_terrain || !h_aspect || !h_wind || !h_forest || !h_slope || !h_alti || !h_fuego_propaga) // || !h_real_fire)
	{
		printf("Mapa Constructor: Error alocando matrices en host \n");
		exit(-1);
	}

	if (!d_terrain || !d_forest || !d_aspect || !d_terrainnew || !d_wind || !d_alti || !d_fuego_propaga) // || !d_real_fire)
	{
		printf("Mapa Constructor: Error alocando matrices en device \n");
		exit(-1);
	}
}


// esta funcion se llama cuando se lee del archivo el fuego inicial, entonces lo proceso
// para obtener las coordenadas del fuego. 
int Obtener_coordenadas_fuego_inicial(float *h_terrain, int filas, int cols, int *filaIni, int *colIni)
{
	int f, c;
	int flag = 1;

	for (f=0; f < filas; f++) {
		for(c=0; c < cols; c++)
		{
			if (h_terrain[f*cols + c] == 1.0)
			{
				*filaIni = f;
				*colIni = c;
				flag = 0;
				break;
			}
		}
		if (!flag) 
			break;  // sale si salimos del for anterior por !flag
	}

	return 0;  
}


// uso el mapa de referencia: h_reference_map, que fue o random o sintentico
void Mapa::Obtener_coordenadas_fuego_referencia(int nref)
{
	int f, c ;
   	h_indicesfuegoRef.clear();
   	for (f=0; f < ROWS; f++)
		for(c=0; c < COLS; c++)
			if (h_reference_map[nref][f*COLS + c] == 1.0 || h_reference_map[nref][f*COLS + c] == -1.0)
		   	{
				h_indicesfuegoRef.push_back(f*COLS+c);	  
		   	}	

   	if(h_indicesfuegoRef.size()==0)  {
		std::cout << "no hay sitios quemados...exit!" << std::endl;exit(1);
   	} 
}


// inicializa el mapa con una celda quemada aleatoria PERO dentro del area quemada del mapa de referencia (sea real o simulada)
int inicializar_Mapa_Random(float *h_terrain, float *h_vege, int filas, int cols, std::vector<int> &h_indicesfuegoRef,int *x, int *y)
{

	srand(time(NULL));

	int unodelosquemados=h_indicesfuegoRef[rand() % h_indicesfuegoRef.size()];

	while (h_vege[unodelosquemados] == 0.0)
	{
		unodelosquemados=h_indicesfuegoRef[rand() % h_indicesfuegoRef.size()];
	}


	for (int fi = 0; fi < filas; fi++){
		for(int col = 0; col < cols; col++){
			h_terrain[fi * cols + col] = (fi * cols + col==unodelosquemados) ? 1.0: 0.0; 
			
			if(fi * cols + col==unodelosquemados) 
			{
				*x = fi;
				*y = col;
			}
		}
	}
	return 1;
}



//Inicio el fuego en UN punto incendiable hveg=1 o hveg=2 lo mas cercano posible al definido en Parameters.h como (XIGNI,YIGNI) 
void inicializar_Mapa_Determinista(float *h_terrain, float *h_vege, int filas, int cols,int *x,int *y)
{

	int f,c;

	// elijo valores al azar hasta caer en una celda quemable
	c = XIGNI;
	f = YIGNI;

	//en caso en que la celda no sea quemable me muevo en diagonal hasta encontrar una quemable	
  	while (h_vege[f*cols+c] == 0.0) 
  	{
		f=f+1;
		c=c+1;
	}
  
	for (int fi = 0; fi < filas; fi++) {
		for(int col = 0; col < cols; col++){
			h_terrain[fi * cols + col] = ((fi == f) && (col == c)) ? 1.0: 0.0;	
		}
	} 


  *x=f;
  *y=c;
}

// Inicializa todos los mapas necesarios y el mapa inicial. 
void Mapa::Init_All_Maps(int filas, int cols)
{
  
	// leo de disco todos los mapas que describen el medio ambiente
	GetMap(VEGETATION_MAP, h_vege, filas, cols);
	CpyHostToDevice(d_vege, h_vege, filas, cols);
      
       // leo mapas en cpu y paso a gpu MAPA de VIENTO
	GetMap(WIND_MAP, h_wind, filas, cols);
	CpyHostToDevice(d_wind, h_wind, filas, cols);
	
	// leo mapas en cpu y paso a gpu MAPA de COMBUSTIBLE
	GetMap(FOREST_MAP, h_forest, filas, cols);
	CpyHostToDevice(d_forest, h_forest, filas, cols);
	
	// leo mapas en cpu y paso a gpu MAPA de ORIENTACION DE LA PENDIENTE
	GetMap(ASPECT_MAP, h_aspect, filas, cols);
	CpyHostToDevice(d_aspect, h_aspect, filas, cols);
		
	// leo mapas en cpu y paso a gpu MAPA de PENDIENTE
	GetMap(SLOPE_MAP, h_slope, filas, cols);
	CpyHostToDevice(d_slope, h_slope, filas, cols);
		
	// leo mapas en cpu y paso a gpu MAPA de ALTITUD
	GetMap(ALTI_MAP, h_alti, filas, cols);
	CpyHostToDevice(d_alti, h_alti, filas, cols);

	dim3 dimBlock(NTHREADS_X, NTHREADS_Y);
	dim3 dimGrid(ROWS / NTHREADS_X, COLS / NTHREADS_Y);
	
	KERNEL_calculate_psi_value<<<dimGrid, dimBlock>>>(d_aspect, d_slope,filas,cols); 
	checkCUDAError("Launch failure: ");

	/*Inicializo mapa de probabilidades en cero*/
	for (int fi = 0; fi < ROWS; fi++)
	{
	 for(int col = 0; col < COLS; col++)
	 {
          h_pmap[fi * COLS + col] = 0.0; //inicializo todas las probabilidades en cero
	 }
	}
	CpyHostToDevice(d_pmap, h_pmap, filas, cols);
	
}
	
void Mapa::Inicializar_datos_celda(DatosCelda *h_mapa_datos)
{
	for (int fi = 0; fi < ROWS; fi++)
	{
	 for(int col = 0; col < COLS; col++)
	 {
          h_mapa_datos[fi * COLS + col].timeFireOn = 0.0; //inicializo todos los tiempos en cero
	 }
	}		  
}

void Mapa::Init_Initial_Map(int *x, int *y) //Esta funcion se refiere a la inicializacion de las SIMULACIONES
{
	// MAPA INICIAL EN h_terrain HAY DOS POSIBILIDADES: son excluyentes!
	//las simulaciones comienzan desde un punto fijo determinado (XIGNI,YIGNI) o comienzan desde un punto elegido aleatoriamente dentro del area quemada 
	
	if (IGNICION_SIMS_DETERMINISTA)   // el fuego comienza en un punto conocido (en parameters.h)
	{
	  	inicializar_Mapa_Determinista(h_terrain, h_vege, ROWS, COLS,x,y);
	 
	}
	else
	{ // se inicializa un mapa random pero dentro de los puntos quemados en el de referencia
		inicializar_Mapa_Random(h_terrain, h_vege, ROWS, COLS, h_indicesfuegoRef,x,y);
	}


 	Inicializar_datos_celda(h_mapa_datos);
    
   	HANDLE_ERROR(cudaMemcpy(d_mapa_datos,h_mapa_datos,sizeof(DatosCelda)*filas*cols, cudaMemcpyHostToDevice));
		 	
	CpyHostToDevice(d_terrain, h_terrain, ROWS, COLS);

}
	



int celda_quemable(float *h_vege, int celdax, int celday)
{
	return (h_vege[celday * COLS + celdax] == 1.0);
}



// quema una celda para iniciar la quema del mapa sintetico de referencia
void Mapa::Iniciar_Punto_de_Ignicion_Mapa_Sintetico_de_Referencia(int *x, int *y, int nref)
{
	if (IGNICION_REFERENCIA_DETERMINISTA)
	{
		// inicializar el punto de ignición usando XIGNI YIGNI
		inicializar_Mapa_Determinista(h_reference_map[nref], h_vege, ROWS, COLS,x,y);
	
	}
	else {
		// random x e y y le pongo 1.0 en esa celda
		int celdax, celday;

		srand(time(NULL));
		celdax = rand() % COLS;
		celday = rand() % ROWS;

		while (!celda_quemable(h_vege, celdax, celday)) {
			celdax = rand() % COLS;
			celday = rand() % ROWS;
		}


		for (int fi = 0; fi < ROWS; fi++)
		{
			for(int col = 0; col < COLS; col++){
				h_reference_map[nref][fi * COLS + col] = ((celday == fi) && (celdax == col)) ? 1.0: 0.0;
			}
		}
		
		// seteo los valores de la celda de inicio ya que los necesito fuera
		*x = celdax;
		*y = celday;
	}
}




/*Inicializo Mapa de Referencia que es el mapa con el que voy a comparar las simulaciones*/
/* puede llegar a ser con un mapa simulado (sintentico) o con el mapa leido desde el disco REAL_FIRE_MAP */
void  Mapa::Init_Reference_Map(int *x, int *y, int modulo, int nada, int nreferencia, vector<int> &h_indices_referencia)
{
 
	if(USAR_MAPA_SINTETICO) 	/*Hago la simulacion de referencia*/ 
	{

		Individuo_T individuo_params_iniciales;
		// Quema la primer celda: o random o determinista
		Iniciar_Punto_de_Ignicion_Mapa_Sintetico_de_Referencia(x, y, nreferencia);


		/* copio h_reference_map de turno que se inicializa en el renglon anterior a device d_terrain*/
		CpyHostToDevice(d_terrain, h_reference_map[nreferencia], filas, cols);
		
		//	parametros.init(0); //inicializo con parametros conocidos ver Parameters.h
		// REEMPLAZO parametros.init(0) POR LOS VALORES CONOCIDOS
 		individuo_params_iniciales.beta_cero 	=  beta_cero_best;
 		individuo_params_iniciales.beta_forest 	=  beta_forest_best;
 		individuo_params_iniciales.beta_aspect 	=  beta_aspect_best;
 		individuo_params_iniciales.beta_slope 	=  beta_slope_best;
 		individuo_params_iniciales.beta_wind 	=  beta_wind_best;

		//	int nroPeliRef=nreferencia*CANT_MAPAS_PELI;	
		if(IMPRIMIR){
 			cout << "VALORES DE LOS PARAMETROS PARA EL MAPA DE REFERENCIA: " << endl;
			cout << individuo_params_iniciales.beta_cero << "  " <<   individuo_params_iniciales.beta_forest << "  " << individuo_params_iniciales.beta_aspect << "  " ;
			cout << individuo_params_iniciales.beta_slope << "  " <<  individuo_params_iniciales.beta_wind << endl ;
		}
		
		char nombre[]="paramsfuegoINICIAL";
		PrintParamsRef(nombre,* x,* y,individuo_params_iniciales);

		Inicializar_datos_celda(h_mapa_datos);
		cudaMemcpy(d_mapa_datos,h_mapa_datos,sizeof(DatosCelda)*filas*cols, cudaMemcpyHostToDevice);

		int iteracion = 0;
	
	
		while ((iteracion < T_RUN)){

			Fire_Propagation_Fuego_de_Referencia(nreferencia+iteracion, individuo_params_iniciales); 
			
			iteracion++;

		}
		
		/* pasar desde d_terrain a h_reference_map */
		CpyDeviceToHost(h_reference_map[nreferencia], d_terrain, filas, cols);
	    if(nreferencia==0)SaveMap("mapa_referencia_0.map",h_reference_map[nreferencia], ROWS, COLS);   
	    if(nreferencia==1)SaveMap("mapa_referencia_1.map",h_reference_map[nreferencia], ROWS, COLS);
	    if(nreferencia==2)SaveMap("mapa_referencia_2.map",h_reference_map[nreferencia], ROWS, COLS);
		
	}
	else {
		// leo el mapa real y lo dejo en h_reference_map 
		GetMap(REAL_FIRE_MAP, h_reference_map[0], filas, cols);

	}

	// en ambos casos dejo en device el mapa de referencia para calcular fitness en paralelo
	CpyHostToDevice(d_reference_map, h_reference_map[nreferencia], filas, cols);
	
	// se obtienen las celdas del fuego de referencia que están quemadas
	Obtener_coordenadas_fuego_referencia(nreferencia);
	h_indices_referencia = h_indicesfuegoRef;
	
	
}






// Copy Functions

/* transfer from CPU to GPU memory */
void Mapa::CpyHostToDevice(float *d_mapa, float *h_mapa, int filas, int cols )
{
     HANDLE_ERROR(cudaMemcpy(d_mapa,h_mapa, sizeof(float)*filas*cols, cudaMemcpyHostToDevice));
}


/* transfer from GPU to CPU memory */
void Mapa::CpyDeviceToHost(float *h_mapa, float *d_mapa, int filas, int cols )
{
    HANDLE_ERROR(cudaMemcpy(h_mapa, d_mapa, sizeof(float)*filas*cols, cudaMemcpyDeviceToHost));
}



/* ENTRADA Y SALIDA DE MAPAS */
void Mapa::GetMap(const char * nombre, float *mapa, int filas, int cols)
{
	FILE *input = fopen(nombre,"r");

	if (input == NULL)
	{
		printf("Get Map error: no abre archivo %s \n", nombre);
		exit(-1);
	}

	int i,j, dato;

	char line[MAX];
	for(i=0; i < 6; i++)
	{
		fgets(line, MAX, input);
	}

	for(i = 0; i < filas; i++)
	{
		for(j = 0; j < cols; j++)
		{
			fscanf(input, "%d", &dato);
			mapa[i*cols + j] = (float)dato;
		}
		
	}
	fclose(input);

}





void Mapa::SaveMap(char * nombre, float *mapa, int filas, int cols)
{
	FILE *output = fopen(nombre,"w");

	if (output == NULL)
	{
		printf("Save Map error: no abre archivo %s \n", nombre);
		exit(-1);
	}

	if (IMPRIMIR)
		printf("Writing final map: %s \n", nombre);

	int i,j;
	float dato;
	for(i = 0; i < filas; i++) {
		for(j = 0; j < cols; j++) {
			dato = mapa[i*cols + j];
			fprintf(output,"%.2f ",dato); 
			//"%.0f ", dato);
		}
		fprintf(output, "\n");
	}
	fclose(output);

}


void Mapa::PrintParamsRef(char * nombre,int x,int y, Individuo_T individuo_params_iniciales)
{
	
	FILE *output = fopen(nombre,"w");

        if (output == NULL)
	{
		printf("SaveList error: no abre archivo %s \n", nombre);
		exit(-1);
	}

		
	float beta_cero=individuo_params_iniciales.beta_cero ;
	float beta_forest=individuo_params_iniciales.beta_forest;
	float beta_aspect=individuo_params_iniciales.beta_aspect;
	float beta_slope=individuo_params_iniciales.beta_slope;
	float beta_wind=individuo_params_iniciales.beta_wind;


	fprintf(output,"MAPA de REFERENCIA\n");
	fprintf(output,"%s %s %s %s %s %s %s\n", "beta_cero","beta_forest", "beta_aspect","beta_slope","beta_wind", "xinicial","yinicial");
 	fprintf(output," %f %f %f %f %f %d %d\n",beta_cero,beta_forest,beta_aspect,beta_slope,beta_wind,x,y); 


	fclose(output);

}


/*para imprimir los mapas de probabilidad en funcion dl tiempo*/
void Mapa::print_probability_progress(int iteration)
{
    char nombre1[] = "proba";
    char vector[5], vector2[5] ;
    int temporal, iteration2 = iteration;
    int Base = 48,  cont=0, i, j;
  
    // armo un vector INVERTIDO con los numeros, caracteres
    for(i=0; iteration2!=0; i++){
      temporal = iteration2%10;
      iteration2 = (iteration2-temporal)/10;
      vector[i]= (char)(temporal+Base);
      cont++;
    } 
    for (i = cont; i < 4; i++)
      vector[i] = '0';
    // invierto el vector
    for (j = 0; j < 4 ; j++)
      vector2[j] = vector[4-1-j];
  
    vector2[4] = '\0'; 
    strcat(nombre1, vector2);
  
  
    // tengo que traer desde device a host d_pmap
    CpyDeviceToHost(h_pmap, d_pmap, ROWS, COLS);

    // le pego al nombre creado el nombre del directorio donde quiero los mapas almacenados
    char nombre2[] = DIR_FIRE_PROGRESS;
    strcat(nombre2, nombre1);
    SaveMap(nombre2,h_pmap, ROWS, COLS);    
  
}


/* para obtener las distintas lineas de avance del fuego */
void Mapa::print_fire_progress(int iteration)
{
    char nombre1[] = "mapa";
    char vector[5], vector2[5] ;
    int temporal, iteration2 = iteration;
    int Base = 48,  cont=0, i, j;
  
    // armo un vector INVERTIDO con los numeros, caracteres
    for(i=0; iteration2!=0; i++){
      temporal = iteration2%10;
      iteration2 = (iteration2-temporal)/10;
      vector[i]= (char)(temporal+Base);
      cont++;
    } 
    for (i = cont; i < 4; i++)
      vector[i] = '0';
    // invierto el vector
    for (j = 0; j < 4 ; j++)
      vector2[j] = vector[4-1-j];
  
    vector2[4] = '\0'; 
    strcat(nombre1, vector2);
  
  
    // tengo que traer desde device a host d_terrain
    CpyDeviceToHost(h_terrain, d_terrain, ROWS, COLS);
  
    // le pego al nombre creado el nombre del directorio donde quiero los mapas almacenados
    char nombre2[] = DIR_FIRE_PROGRESS;
    strcat(nombre2, nombre1);
    SaveMap(nombre2,h_terrain, ROWS, COLS);    
  
}




// el fitness depende del fuego propagado y del mapa de referencia
float Mapa::calcular_fitness_paralelo(int filas, int cols)
{
    float fitness_unaref=0;
   // float fitness_totref=0;
    thrust::host_vector<float> fitness_vector(CANT_MAPAS_REFERENCIA);
		// calculo el histograma en el intervalo real [min,max]
    
    // se calcula el fitness con todos los mapas de referencia y los fitness se almacenan en fitness_vector
    for(int nr = 0; nr < CANT_MAPAS_REFERENCIA; nr++){
    	
    	CpyHostToDevice(d_reference_map,h_reference_map[nr], filas, cols ); //cargo en device un mapa de referencia
		// mapa de referencia y mapa propagado para el fitness
    	fitness_unaref=fitness.Calcular_Fitness_paralelo(filas, cols, d_reference_map, d_terrain, d_vege);
    
       	//fitness_totref=fitness_totref+fitness_unaref;
    
    	fitness_vector[nr]=fitness_unaref;
    }

    // se calcula el minimo y se retorna ese minimo
    float minimo=*std::min_element(fitness_vector.begin(),fitness_vector.end());
    return(minimo);
}




/* Funciones Algoritmo Genetico */
int Mapa::Evaluar_Individuo(Individuo_T *d_poblacion, int indice, int flag_imprimir_mejor_mapa, int numero_generacion)
{

	/* intero con un individuo y propago en todo el mapa */
	int paso_simulacion = 0;
	int verificar_propagacion = 1;

	/* flag para parar de iterar si el fuego no propaga */
	int *h_fuego_propaga = (int*)malloc(sizeof(int));
	int *d_fuego_propaga;
	HANDLE_ERROR(cudaMalloc((void**)&d_fuego_propaga, sizeof(int)));

  	//nuevo agregado para que vuelva a inicializar el mapa fuera del loop en main, sino propagaba del ultimo mapa y no dan bien las sims
	if(flag_imprimir_mejor_mapa){
	 int x=XIGNI;
	 int y=YIGNI;
	 Init_Initial_Map(&x,&y);
	}

	if (verificar_propagacion) {
		h_fuego_propaga[0] = 1;
		HANDLE_ERROR(cudaMemcpy(d_fuego_propaga, h_fuego_propaga, sizeof(int), cudaMemcpyHostToDevice));
	}

	while ((paso_simulacion < T_RUN) && (h_fuego_propaga[0]) ) {
    
        	paso_simulacion++;
	 	  
			verificar_propagacion = (paso_simulacion % FUEGO_PROPAGA_NUMERO) == 0;
			
			/*El primer arg es para que no sean iguales las sims*/
			Fire_Propagation(paso_simulacion+indice+numero_generacion, h_fuego_propaga, d_fuego_propaga, verificar_propagacion, d_poblacion, indice);
	
			if (verificar_propagacion) {
				HANDLE_ERROR(cudaMemcpy(h_fuego_propaga, d_fuego_propaga, sizeof(int), cudaMemcpyDeviceToHost));
			}

	}


	if (flag_imprimir_mejor_mapa) {
		CpyDeviceToHost(h_terrain, d_terrain, filas, cols);
    		SaveMap("mapa_final.map",h_terrain, ROWS, COLS);   
	}	

	free(h_fuego_propaga);
	cudaFree(d_fuego_propaga);

	return 0;
}


 int Mapa::Evaluar_Poblacion(Individuo_T *d_poblacion, Fitness_T *h_fitnesses, int numero_generacion){

    // recibe el mapa y para cada individuo envia a 
    int ind;
    double fit;
    int x,y;


    for (ind = 0; ind  < CANT_INDIVIDUOS; ind++) {


	Init_Initial_Map(&x, &y);


        // mando a Mapa que es donde tengo los mapas y toda la funcinalidad de la propagacion
	Evaluar_Individuo(d_poblacion, ind, 0,numero_generacion);  // 0 para que no imprima el mapa
        fit = calcular_fitness_paralelo(ROWS,COLS);

        /* Ver como organizo los ides*/
        h_fitnesses[ind].id = ind;
        h_fitnesses[ind].fitness = fit;
   }

   return 0;
}
